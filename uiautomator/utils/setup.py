#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP
# Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Kailiang Li
# Created Date: 6/4/2018
# Updated Date: 6/22/2018
#
# Description: This module implements automated scripts running on different
#              platforms.
# ---------------------------------------------------------------------------
import os
import time
from abc import abstractmethod
import sys
sys.path.append('../core')
sys.path.append('../utils')
file_path = os.path.abspath(os.path.dirname(__file__))
from settings import *

"""
Instructions:
    1、What is this pattern about?
       The Factory Method pattern can be used to create an interface for a
       method, leaving the implementation to the class that gets instantiated.
    2、What does this example do?
       The code shows a way to execute setUp() method in different boards :
       mek_8q、sabresd_6d and mek_8q_car；
    3、What is OperationFactory?
       OperationFactory is the factory that constructs a localizer depending
       on the board chosen. The localizer object will be an instance from a
       different class according to the board localized. However, the main
       code does not have to worry about which localizer will be instantiated,
       since the method"setUp" will be called in the same way independently of
       the board.
Note:
    1. The parameters in setUp() function should be written to configuration
       files.
"""


class BoardFactory(object):
    """Create an interface"""

    @abstractmethod
    def setUp(self):
        pass

    @abstractmethod
    def tearDown(self):
        pass


class Mek8Q(BoardFactory):
    """Create an entity class Mek8Q that implements the BoardFactory
    interface."""

    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno

    def setUp(self):
        print("8.1.0 android system is being started...")
        self.d.wakeup()
        self.d.press.home()
        time.sleep(2)
        pass
        # The value of a variable in d.swipe() function should be written
        # to the configuration file.
        self.d.swipe(400, 400, 400, 20, steps=10)
        time.sleep(2)
        self.d(text="Settings").click.wait()

    def tearDown(self):
        self.d.press.back()
        self.d.press.home()
        time.sleep(1)
        print("tearDown 8.1.0 android system ")


class Sabresd6dq(BoardFactory):
    """Create an entity class Sabresd6dq that implements the BoardFactory
    interface."""

    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno

    def setUp(self):
        print("8.0.0 android system is being started...")
        self.d.wakeup()
        self.d.press.home()
        time.sleep(2)
        self.d.swipe(400, 400, 400, 20, steps=10)
        time.sleep(2)
        self.d(text="Settings").click.wait()

    def tearDown(self):
        self.d.press.back()
        self.d.press.home()
        time.sleep(1)
        print("tearDown 8.0.0 android system ")


class Mek8qCar(BoardFactory):
    """Create an entity class Mek8qCar that implements the BoardFactory
    interface."""

    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno
        self.settingscommon = SettingsCommon(self.d, self.serialno)

    def setUp(self):
        print("8.1.0 android car system is being started...")
        if self.d(text="Owner",
                  resourceId=self.settingscommon.SYSTEMUI_USER_NAME_ID)\
                .exists:
            self.d(
                resourceId=self.settingscommon.SYSTEMUI_SETTINGS_BUTTON_ID)\
                .click.wait()
        else:
            self.d.long_click(930, 1000)
            time.sleep(5)
            if self.d(text="Owner",
                      resourceId=self.settingscommon.SYSTEMUI_USER_NAME_ID)\
                    .exists:
                temp = self.settingscommon.SYSTEMUI_SETTINGS_BUTTON_ID
                self.d(
                    resourceId=temp).click.wait()
                if self.d(text="Search settings",
                          resourceId="android.widget.TextView").exists:
                    print("Now you enter settings primary UI\n")
                else:
                    print("Please check if you are in settings primary UI\n")

    def tearDown(self):
        self.d.long_click(930, 1000)


class EVKMX6SL(BoardFactory):
    """Create an entity class EVKMX6SL that implements the BoardFactory
    interface."""
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno

    def isOk(self):
        if self.d(text="OK").exists:
            self.d(text="OK").click.wait()

    def setUp(self):
        print("8.0.0 android system is being started...")
        self.isOk()
        self.d.wakeup()
        self.d.press.home()
        self.d(description="Apps").click.wait()
        self.isOk()
        time.sleep(2)
        time.sleep(2)

    def tearDown(self):
        self.d.press.back()
        self.d.press.home()
        time.sleep(1)
        print("tearDown 8.0.0 android system ")


class OperationFactory(object):
    """Create an OperationFactory to generate objects of entity classes based
    on given board information."""

    @staticmethod
    def createOperate(productname, d, serialno):
        """
        :param productname: Board information
        :param d: Device driver
        :param serialno: Serial number of Android equipment
        :return: Specify board object
        """
        boarddict = dict(mek_8q=Mek8Q,
                         sabresd_6dq=Sabresd6dq,
                         mek_8q_car=Mek8qCar,
                         evk_6sl=EVKMX6SL)
        board = BoardFactory()
        if(boarddict.__contains__(productname)):
            board = boarddict[productname](d, serialno)
        return board


# Using OperationFactory, the object of entity class is obtained by passing
# type information.

def set_up(productname, d, serialno):
    board = OperationFactory.createOperate(productname, d, serialno)
    board.setUp()


# Using OperationFactory, the object of entity class is obtained by passing
# type information.
def tear_down(productname, d, serialno):
    board = OperationFactory.createOperate(productname, d, serialno)
    board.tearDown()
