#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Fiona & lkl
# Created Date: 3/23/2018
# Updated Date: 3/29/2018
#
# Description:
# clearAll: Clear all the opened applications before executing other test scripts
# clearGallery: Clear all pictures and videos in gallery (for UI is not in sync with real storage).
# -----------------------------------------------------------------------------------------------
import time

class Clear:
    
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno
        
    def clearAll(self):
        try:
            self.d.wakeup()
            self.d.swipe(400, 400, 400, 20, steps=10)
            self.d.press.home()
            self.d.press.recent()
            time.sleep(3)
            if self.d(text="No recent items").exists:
                self.d.press.back()
            # Add else by Fiona for correct logic
            else:
                while (self.d(text="CLEAR ALL").exists == False):
                    self.d.swipe(300, 300, 300, 400, steps=10)  # swipe down until the "CLEAR ALL" button appear
                self.d(text="CLEAR ALL").click()
            time.sleep(2)
        except Exception as e:
            print(e)
            raise

    def clearGallery(self):
        try:
            self.d.wakeup()
            self.d.swipe(400, 400, 400, 20, steps=10)
            self.d.press.home()
            self.d(text="Gallery").click.wait()
            time.sleep(2)
            if self.d(text="Camera", clickable="true").exists: #camera funtion clickable means there is no ablums
                self.d.press.back()
            else:
                self.d(className="android.widget.ImageButton").click()
                self.d(text="Select album").click()
                self.d(resourceId="com.android.gallery3d:id/selection_menu").click.wait()
                self.d(text="Select all").click()
                self.d(resourceId="com.android.gallery3d:id/action_delete").click.wait()
                time.sleep(1)
                self.d(text="OK").click.wait()
                time.sleep(1)
                self.d.press.back()
            time.sleep(2)
        except Exception as e:
            print(e)
            raise

