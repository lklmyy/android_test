#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.
#
# Author: Faye
# Created Date: 5/09/2018
# Updated Date: 6/21/2018
#
# Description:
# 1. This script contains all the common function
# -----------------------------------------------------------------------------------------------
import os
import subprocess
import time
import re


class Common:
    def __init__(self, d, serial_no):
        self.d = d
        self.serial_no = serial_no

    def is_adb(self):
        try:
            cmd = u'adb devices'
            p = subprocess.Popen(cmd,
                                 shell=True,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)
            r = p.stdout.read().decode('utf-8').strip()
            p.stdout.close()
            return r
        except Exception as e:
            print(e)
            raise

    def check_installed_apk(self, package_name):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell pm list packages | grep ' \
                  + package_name
            p = subprocess.Popen(cmd,
                                 shell=True,
                                 stdout=subprocess.PIPE)
            r = p.stdout.read().decode('utf-8').strip()
            p.stdout.close()
            return r
        except Exception as e:
            print(e)
            raise

    def install_apk(self, filename):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' install %s' % filename
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def uninstall_apk(self, package_name):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' uninstall ' \
                  + package_name
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def push_file(self, src, dst):
        try:
            cmd = u'adb -s ' + self.serial_no + ' push %s %s' % (src, dst)
            r = os.system(cmd)  # type: int
            return r
        except Exception as e:
            print(e)
            raise

    def pull_file(self, src, dst):
        try:
            cmd = u'adb -s ' + self.serial_no + ' pull %s %s' % (src, dst)
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def refresh_file(self, file_name):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell am broadcast ' \
                    '-a android.intent.action.MEDIA_SCANNER_SCAN_FILE ' \
                    '-d file://' \
                  + file_name
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            r = p.stdout.read().decode('utf-8')
            p.stdout.close()
            return r
        except Exception as e:
            print(e)
            raise

    def restart_tcp(self, port):
        try:
            cmd = u'adb -s ' + self.serial_no + ' tcpip ' + port
            tcp_mode = os.popen(cmd).read().strip()
            return tcp_mode
        except Exception as e:
            print(e)
            raise

    def restart_usb(self):
        try:
            cmd = u'adb -s ' + self.serial_no + ' usb'
            usb_mode = os.popen(cmd).read().strip()
            return usb_mode
        except Exception as e:
            print(e)
            raise

    def adb_connect(self, ip_address):
        try:
            cmd = u'adb -s ' + self.serial_no + ' connect ' + ip_address
            r = os.popen(cmd).read().strip()
            return r
        except Exception as e:
            print(e)
            raise

    def adb_kill_server(self):
        try:
            r = os.system(u'adb -s ' + self.serial_no + ' kill-server')
            return r
        except Exception as e:
            print(e)
            raise

    def get_ip_address(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell ifconfig eth0 | grep "inet addr:"'
            r = os.popen(cmd).readline().strip()
            i = re.split(" |:", r)[2]
            return i
        except Exception as e:
            print(e)
            raise

    def get_serial_no(self):
        try:
            cmd = u'adb -s ' + self.serial_no + ' shell getprop ro.serialno'
            r = os.popen(cmd).read().strip()
            return r
        except Exception as e:
            print(e)
            raise

    def get_product_name(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell getprop ro.product.name'
            r = os.popen(cmd).read().strip()
            return r
        except Exception as e:
            print(e)
            raise

    def set_rotation_status(self, value):
        try:
            cmd = 'adb -s ' + self.serial_no \
                  + ' shell settings put system accelerometer_rotation ' \
                  + str(value)
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def get_rotation_status(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell settings get system accelerometer_rotation'
            r = os.popen(cmd).read()
            return r
        except Exception as e:
            print(e)
            raise

    def set_rotation_value(self, value):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell settings put system user_rotation ' \
                  + str(value)
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def get_rotation_value(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell settings get system user_rotation'
            r = os.popen(cmd).read()
            return r
        except Exception as e:
            print(e)
            raise

    def set_screen_capture(self, file_name):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell screencap -p ' + str(file_name)
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def open_calculator(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell am start com.android.calculator2/.Calculator'
            r = os.system(cmd)
            return r
        except Exception as e:
            print(e)
            raise

    def open_settings(self, product_name):
        try:
            if product_name == "mek_8q_car":
                if self.d(text="Owner",
                          resourceId="com.android.systemui:id/user_name")\
                        .exists:
                    self.d(
                        resourceId="com.android.systemui:id/settings_button")\
                        .click.wait()
                else:
                    self.d.long_click(930, 1000)
                    time.sleep(5)
                    if self.d(text="Owner",
                              resourceId="com.android.systemui:id/user_name")\
                            .exists:
                        self.d(
                            resourceId="com.android.systemui:id/"
                                       "settings_button")\
                            .click.wait()
                        if self.d(text="Search settings").exists:
                            print("Now you are in settings primary UI.\n")
                        else:
                            print("Check if you are in settings.\n")
            elif product_name == 'mek_8q' or product_name == "sabresd_6dq":
                self.d.wakeup()
                self.d.press.home()
                time.sleep(2)
                self.d.swipe(400, 400, 400, 20, steps=10)
                time.sleep(2)
                self.d(text="Settings").click.wait()
                if self.d(text="Search settings").exists:
                    print("Now you enter settings primary UI.\n")
                else:
                    print("Please check if you are in settings primary UI.\n")
        except Exception as e:
            print(e)
            raise

    def return_home(self, product_name):
        try:
            if product_name == "mek_8q_car":
                self.d.long_click(930, 1000)
            elif product_name == 'mek_8q' or product_name == "sabresd_6dq":
                self.d.press.back()
                self.d.press.home()
        except Exception as e:
            print(e)
            raise

    def install_sound_recorder(self, apk_name):
        try:
            apk_name = 'SoundRecorder.apk'
            r = self.check_installed_apk('SoundRecorder')
            print(r)
            if len(r) == 0:
                file_list = os.listdir('../../resource/')
                for file in file_list:
                    if file == apk_name:
                        apk_name = os.path.join('../../resource/', apk_name)
                        self.install_apk(apk_name)
                        time.sleep(5)
                        print("Successfully installed the apk.\n")
                    else:
                        continue
            else:
                print("Apk already installed, please double-check!\n")
        except Exception as e:
            print(e)
            raise

    # functions from camera
    def change_function(self, on):
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/preview_content")\
                .swipe.right(steps=10)
            self.d(text=on).click.wait()
        except Exception as e:
            print(e)
            raise

    def switch_quality(self, direction, quality):
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/preview_content")\
                .wait.exists(timeout=1000)
            self.d(resourceId="com.android.camera2:id/preview_content")\
                .swipe.right(steps=10)
            self.d(resourceId="com.android.camera2:id/settings_button").click()
            self.d(text="Resolution & quality").click.wait()
            if self.d(text=direction).exists:
                self.d(text=direction).click()
                # try to open front/back picture/video change page
            else:
                print("can't find this direction")
                raise
            if self.d(text=quality).exists:
                self.d(text=quality).click()   # try to change quality
            else:
                print("can't find this quality")
                raise
            self.d.press.back()
            self.d.press.back()
            time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def record_shoot(self, rs, t, describe):
        # if rs=1 it's recording, else it's taking picture
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/shutter_button")\
                .wait.exists(timeout=1000)
            self.d(resourceId="com.android.camera2:id/shutter_button").click()
            if rs == 1:
                time.sleep(2)
                self.d.screenshot(describe+".png")
                time.sleep(t)        # recording for t s
                self.d(resourceId="com.android.camera2:id/shutter_button")\
                    .click.wait()
                # stop recording
            time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def open_camera(self):
        try:
            self.d.wakeup()
            self.d.orientation = "n"   # rotate to natural
            self.d.swipe(400, 400, 400, 20, steps=10)
            self.d.press.home()
            self.d(text="Camera").click.wait()
            time.sleep(2)
            if self.d(text="ALLOW").wait.exists(timeout=2000):
                self.d(text="ALLOW").click.wait()
                time.sleep(2)
                if self.d(text="NEXT").wait.exists(timeout=2000):
                    self.d(text="NEXT").click.wait()
                    time.sleep(2)
                    print("Open camera successfully")
                else:
                    print("Not find NEXT window")
            else:
                print("Not find ALLOW window")
            time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def change_to_back(self):
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle")\
                .click()   # open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:
                # check if there is change option, check 2 cameras
                second = True
                if self.d(description="Front camera").exists:
                    self.d(description="Front camera").click.wait()
                    # set to aim direction
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                second = False
                # there is only back camera no need to check change result
            if second is True:
                # only when there are 2 cameras to check the change result
                if self.d(description="Back camera").exists:
                    # check change result
                    assert True
                    print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")
                else:
                    assert False
        except Exception as e:
            print(e)
            raise

    def change_to_front(self):
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle")\
                .click()   # open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:
                if self.d(description="Back camera").exists:
                    self.d(description="Back camera").click.wait()
                    # set to aim direction option
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                print("Error: There is no second camera")
                raise
            if self.d(description="Front camera").exists:
                # check change result
                assert True
                print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")
            else:
                assert False
        except Exception as e:
            print(e)
            raise
