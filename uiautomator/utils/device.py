import usb

def get_devices():
    devices = []
    devObjs = usb.core.find(True, idVendor=0x18d1)
    for devObj in devObjs:
        devices.append(devObj.serial_number)
    return devices
