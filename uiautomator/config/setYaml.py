#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# ----------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP
# Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Kailiang Li
# Created Date: 6/22/2018
#
# Description: Get a standardized yaml file
# ----------------------------------------------------------------------------

import os
from yaml import load, dump


class YamlDemo:
    def __init__(self):
        self.srcfilelist = ['settingsdraft', 'cameradraft', 'wifibtdraft']
        self.dstfilelist = ['settings.yaml', 'camera.yaml', 'wifibt.yaml']

    def get_data(self, module):
        try:
            if isinstance(module, str):
                for i in range(len(self.srcfilelist)):
                    if module in self.srcfilelist[i]:
                        config_path = os.path.join(os.path.dirname(__file__),
                            self.srcfilelist[i] + '.yaml')
                        with open(config_path, 'rb') as filename:
                            cont = filename.read()
                        yamlFile = load(cont)
                        dumpfile = open(self.dstfilelist[i], 'w')
                        dump(yamlFile, dumpfile)
                        print("Get standard yaml data. Please check in "
                              "current folder.")
                        return yamlFile
                    else:
                        raise FileExistsError("No match to the corresponding "
                                              + module + " file.")
            else:
                raise TypeError(module + "is not a string.")
        except Exception as e:
            print(e)

    def remove_yaml(self):
        for root, dirs, files in os.walk(os.path.dirname(__file__)):
            for name in files:
                if name.endswith('draft.yaml'):
                    os.remove(os.path.join(root, name))
                    print("Delete File: " + os.path.join(root, name))

def main():
    y = YamlDemo()
    y.get_data('settin')
    # y.remove_yaml()

if __name__ == '__main__':
    main()

