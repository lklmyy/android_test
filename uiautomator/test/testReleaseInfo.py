#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Faye Dong
# Created Date: 5/14/2018
#
# Description: Release info test configuration script
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb and uiautomator have been installed on the Ubuntu OS PC you used;
# -----------------------------------------------------------------------------------------------

import sys
sys.path.append('../core')
sys.path.append('../utils')

from releaseinfo import ReleaseInfo
import os
file_path = os.path.abspath(os.path.dirname(__file__))

import device
import unittest
import HTMLTestRunner

from common import Common

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path + '/../result/' + file_name):
    os.makedirs(file_path + '/../result/' + file_name)
os.chdir(file_path + '/../result/' + file_name)

if __name__ == '__main__':

        suite = unittest.TestSuite()

        if os.getenv("sn") == None:
            print('connected devices:', device.get_devices())
            serialno = device.get_devices()[0]
            print("no sn exported, Run on default device:", serialno)
        else:
            serialno = os.getenv("sn")
            print("run on device:", serialno)

        platform = os.popen(u'adb -s ' + serialno + ' shell getprop ro.product.name').read().strip()
        #platform = os.popen(u'adb -s ' + serialno + ' shell getprop ro.vendor.product.name').read().strip()
        print(platform)
        if platform == "mek_8q" or platform == "mek_8q_car":
            # test release info
            #suite.addTest(ReleaseInfo("testCPUfrequency1", serialno))
            suite.addTest(ReleaseInfo("testUbootVersion", serialno))
            suite.addTest(ReleaseInfo("testGPUVersion", serialno))
            suite.addTest(ReleaseInfo("testKernelVersion", serialno))
            suite.addTest(ReleaseInfo("testAndroidVersion", serialno))
            suite.addTest(ReleaseInfo("testEnforceMode", serialno))
            suite.addTest(ReleaseInfo("testEncryptionFromUI", serialno))
            suite.addTest(ReleaseInfo("testEncryptionByCommand", serialno))
        elif platform == "sabresd_6dq":
            # test release info
            suite.addTest(ReleaseInfo("testUbootVersion", serialno))
            #suite.addTest(ReleaseInfo("testGPUVersion", serialno))
            #suite.addTest(ReleaseInfo("testKernelVersion", serialno))
            #suite.addTest(ReleaseInfo("testAndroidVersion", serialno))
            #suite.addTest(ReleaseInfo("testEnforceMode", serialno))
            #suite.addTest(ReleaseInfo("testEncryptionFromUI", serialno))

        if os.getenv("debug") == None:
            filename = file_name + '.html'
            fp = open(filename, 'wb')
            runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=u"Test Result", description=u"Detail Descrpiton", verbosity=2)
            runner.run(suite)
            fp.close()
        else:
            runner = unittest.TextTestRunner(verbosity=2)
            runner.run(suite)
