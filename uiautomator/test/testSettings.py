#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import datetime
import unittest
import HTMLTestRunner
import os
import sys
sys.path.append('../resource/image')
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_path+'/../core')
sys.path.append(file_path+'/../utils')
file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path+'/../result/'+file_name):
    os.makedirs(file_path+'/../result/'+file_name)
os.chdir(file_path+'/../result/'+file_name)
import device
from settings import AndroidSettings

if __name__ == '__main__':
    suite = unittest.TestSuite()
    if os.getenv("sn") is None:
        print('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print("no sn exported, Run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)

    starttime = datetime.datetime.now()
    platform = os.popen(u'adb -s ' + serialno + ' shell \
    getprop ro.product.name').read().strip()
    pass
    """
     Reusability of Scripts Note: If new platform appears,
     some scripts may not be reused,you can change here.
    """
    # test Brightness
    suite.addTest(AndroidSettings("testBrightnessLevel", serialno))
    # suite.addTest(AndroidSettings("testAdaptiveBrightness", serialno))

    # test font button A
    suite.addTest(AndroidSettings("testFontSmallButtonA", serialno))
    suite.addTest(AndroidSettings("testFontBigButtonA", serialno))

    # test  Display - and + button
    suite.addTest(AndroidSettings("testDisplayMinusButton", serialno))
    suite.addTest(AndroidSettings("testDisplayPlusButton", serialno))

    # test font size
    suite.addTest(AndroidSettings("testFontSizeSmall", serialno))
    suite.addTest(AndroidSettings("testFontSizeDefault", serialno))
    suite.addTest(AndroidSettings("testFontSizeLarge", serialno))

    # test display size (There is a defect in the system when the maximum
    # value is displayed)
    suite.addTest(AndroidSettings("testDisplaySizeSmall", serialno))
    suite.addTest(AndroidSettings("testDisplaySizeLarge", serialno))
    suite.addTest(AndroidSettings("testDisplaySizeLarger", serialno))
    suite.addTest(AndroidSettings("testDisplaySizeLargest", serialno))
    if platform == 'mek_8q':
        # test Screen saver ( The Android version is no less than 8.1 )
        suite.addTest(AndroidSettings("testClockAnalog", serialno))
        suite.addTest(AndroidSettings("testClockAnalogNightMode", serialno))
        suite.addTest(AndroidSettings("testClockDigital", serialno))
        suite.addTest(AndroidSettings("testClockDigitalNightMode", serialno))
        suite.addTest(AndroidSettings("testColors", serialno))

    # test developer options
    suite.addTest(AndroidSettings("testDeveloperOptions", serialno))

    # test System Tunner
    suite.addTest(AndroidSettings("testStatusBarAirplaneMode", serialno))

    # test Settings Language
    suite.addTest(AndroidSettings("testLanguage", serialno))

    # test Date & time
    suite.addTest(AndroidSettings("testSetDate", serialno))
    suite.addTest(AndroidSettings("testSetTime", serialno))
    # suite.addTest(AndroidSettings("checkSystemTimeAfterReboot", serialno))
    suite.addTest(AndroidSettings("testSelectTimeZone", serialno))
    suite.addTest(AndroidSettings("testSetMultipleAlarms", serialno))
    suite.addTest(AndroidSettings("testAlarmWakeupDevice", serialno))

    # test screen lock
    suite.addTest(AndroidSettings("testScreenPasswordLock", serialno))
    suite.addTest(AndroidSettings("testPatternScreenLock", serialno))
    suite.addTest(AndroidSettings("testPinLock", serialno))

    # test screen lock
    suite.addTest(AndroidSettings("testTimeOut", serialno))

    # # test Reset App
    # suite.addTest(AndroidSettings("inatsllAPK", serialno))
    # suite.addTest(AndroidSettings("testInstall", serialno))
    # suite.addTest(AndroidSettings("testResetapp", serialno))
    #
    # # test Reset Tablet
    # suite.addTest(AndroidSettings("testResetTablet", serialno))
    # suite.addTest(AndroidSettings("testAPPNumber", serialno))
    #
    # suite.addTest(AndroidSettings("copyFiles2", serialno))

    endtime = datetime.datetime.now()
    print((endtime - starttime).seconds)

    if os.getenv("debug") is None:
        # if platform == 'mek_8q':
        #     filename = file_name + 'mek_8q.html'
        # elif platform == 'sabresd_6dq':
        #     filename = file_name + 'sabresd_6dq.html'
        # elif platform == 'mek_8q_car':
        #     filename = file_name + 'mek_8q_car.html'
        # else:
        #     # Add new platform here
        #     pass
        #     raise Exception("The platform has been updated, please add new
        #     platform information")
        filename = file_name + platform + '.html'
        fp = open(filename, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=u"Settings \
        Test Result", description=u"Detail Descrpiton", verbosity=2)
        runner.run(suite)
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
