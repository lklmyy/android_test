import sys
import os
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_path+'/../core')
sys.path.append(file_path+'/../utils')
import device  # noqa
import unittest  # noqa
import HTMLTestRunner  # noqa
from camerastress import Prepare  # noqa
from camerastress import BackcamcorderStress  # noqa
from camerastress import BackcameraStress  # noqa
from camerastress import FrontcamcorderStress  # noqa
from camerastress import FrontcameraStress  # noqa
from camerastress import Longtimestress  # noqa

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path+'/../result/'+file_name):
    os.makedirs(file_path+'/../result/'+file_name)
os.chdir(file_path+'/../result/'+file_name)

if __name__ == "__main__":
    suite = unittest.TestSuite()
    if os.getenv("sn") is None:
        print('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print("no sn exported, Run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)

    platform = os.popen(u'adb -s ' + serialno +
                        ' shell getprop ro.product.name').read().strip()
    if platform == 'mek_8q':
        suite.addTest(Prepare("preparation", serialno))       # prepare
        suite.addTest(BackcamcorderStress("testBackChangeCVStressN", serialno))

        suite.addTest(BackcamcorderStress("testBackCamcorderRecordingStress720", serialno))
        suite.addTest(BackcameraStress("testBackCameraCaptureStress1M", serialno))

        suite.addTest(Longtimestress("OvernightStress", serialno))

    elif platform == 'sabresd_6dq':
        suite.addTest(Prepare("preparation", serialno))       # prepare

        suite.addTest(BackcamcorderStress("testBackChangeCVStressN", serialno))
        suite.addTest(BackcamcorderStress("testBackCamcorderQualityStress", serialno))
        suite.addTest(BackcamcorderStress("testBackCamcorderRecordingStress1080", serialno))

        suite.addTest(BackcameraStress("testBackCameraQualityStress", serialno))
        suite.addTest(BackcameraStress("testBackCameraCaptureStress5M", serialno))

        suite.addTest(FrontcamcorderStress("testFrontChangeCVStressN", serialno))
        suite.addTest(FrontcamcorderStress("testFrontCamcorderQualityStress", serialno))
        suite.addTest(FrontcamcorderStress("testFrontCamcorderRecordingStress", serialno))

        suite.addTest(FrontcameraStress("testFrontCameraQualityStress", serialno))
        suite.addTest(FrontcameraStress("testFrontCameraCaptureStress", serialno))

        suite.addTest(Longtimestress("OvernightStress", serialno))

    if os.getenv("debug") is None:
        filename = file_name + '.html'
        fp = open(filename, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                                               title=u"CameraStress Test Result",
                                               description=u"CameraStress Test",
                                               verbosity=2)
        runner.run(suite)
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
