#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc..
# Author: Faye Dong
# Created Date: 5/14/2018
#
# Description: OTA update test configuration script
# -----------------------------------------------------------------------------------------------
# NOTES:
#  1. Install python3, pyusb, uiautomator before testing;
# -----------------------------------------------------------------------------------------------

import sys
sys.path.append('../core')
sys.path.append('../utils')

from ota import OTA # noqa
import os # noqa
file_path = os.path.abspath(os.path.dirname(__file__))

import device # noqa
import unittest # noqa
import HTMLTestRunner # noqa

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path + '/../result/' + file_name):
    os.makedirs(file_path + '/../result/' + file_name)
os.chdir(file_path + '/../result/' + file_name)

if __name__ == '__main__':

        suite = unittest.TestSuite()

        if os.getenv("sn") is None:
            print('connected devices:', device.get_devices())
            serialno = device.get_devices()[0]
            print("no sn exported, Run on default device:", serialno)
        else:
            serialno = os.getenv("sn")
            print("run on device:", serialno)

        # test OTA update function
        suite.addTest(OTA("testOTAfromUI", serialno))
        # suite.addTest(OTA("testDiffOTAfromUI", serialno))

        if os.getenv("debug") is None:
            filename = file_name + '.html'
            fp = open(filename, 'wb')
            runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                                                   title=u"Test Result",
                                                   description=u"Detail "
                                                               u"Descrpiton",
                                                   verbosity=2)
            runner.run(suite)
            fp.close()
        else:
            runner = unittest.TextTestRunner(verbosity=2)
            runner.run(suite)
