#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import sys
import os
sys.path.append('../core')
sys.path.append('../utils')
import device # noqa
import unittest # noqa
import HTMLTestRunner # noqa
from GUI import GUI # noqa


if __name__ == '__main__':
    suite = unittest.TestSuite()
    if os.getenv("sn") is None:
        print ('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print ("no sn exported, run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)

    suite.addTest(GUI("testLauncherRotate", serialno))
    suite.addTest(GUI("testHomeScreenShot", serialno))
    suite.addTest(GUI("testLiveWallPaperScreenshot", serialno))
    suite.addTest(GUI("testSettingsScreenshot", serialno))
    suite.addTest(GUI("testVideoScreenshot", serialno))
    suite.addTest(GUI("testRecorderScreenshot", serialno))

    if os.getenv("debug") is None:
        filename = os.path.basename(__file__) + '.html'
        fp = open(filename, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=u"Test Result",
                                               description=u"Detail Descrpito"
                                                           u"n", verbosity=2)
        runner.run(suite)
        fp.close()
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
