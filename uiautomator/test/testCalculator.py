#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc..
#
# Author: Faye Dong
# Created Date: 6/14/2018
#
# Description: Calculator test configuration script
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Install python3, pyusb, uiautomator before testing;
# -----------------------------------------------------------------------------------------------
import os
import sys
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append('../core')
sys.path.append('../utils')
import device # noqa
import unittest # noqa
import HTMLTestRunner # noqa
from calculator import Calculator # noqa

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path + '/../result/' + file_name):
    os.makedirs(file_path + '/../result/' + file_name)
os.chdir(file_path + '/../result/' + file_name)

if __name__ == '__main__':
    suite = unittest.TestSuite()
    if os.getenv("sn") is None:
        print('connected devices:', device.get_devices())
        serial_no = device.get_devices()[0]
        print("no sn exported, run on default device:", serial_no)
    else:
        serial_no = os.getenv("sn")
        print("run on device:", serial_no)

    suite.addTest(Calculator("testRotation", serial_no))
    suite.addTest(Calculator("testMonkey", serial_no))

    if os.getenv("debug") is None:
        file_name2 = os.path.basename(__file__) + '.html'
        fp = open(file_name2, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                                               title=u"Test Result",
                                               description=u"Description",
                                               verbosity=2)
        runner.run(suite)
        fp.close()
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)

