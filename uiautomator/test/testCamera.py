import sys
import os
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_path+'/../core')
sys.path.append(file_path+'/../utils')
import clear
import device
import unittest
import HTMLTestRunner
from camera import Prepare
from camera import Backcamcorder
from camera import Backcamera
from camera import Frontcamcorder
from camera import Frontcamera

file_name = os.path.basename(__file__).split('.',1)[0]
if not os.path.exists(file_path+'/../result/'+file_name):
    os.makedirs(file_path+'/../result/'+file_name)
os.chdir(file_path+'/../result/'+file_name)

if __name__ == "__main__":
    suite = unittest.TestSuite()
    if os.getenv("sn") == None:
        print('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print("no sn exported, Run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)


    platform = os.popen(u'adb -s ' + serialno + ' shell getprop ro.product.name').read().strip()
    if platform == 'mek_8q':
        suite.addTest(Prepare("preparation", serialno))       #prepare

        suite.addTest(Backcamcorder("testChangetoBackCamcorder", serialno))       
        suite.addTest(Backcamcorder("testBackCamcorder720preview", serialno))      
        suite.addTest(Backcamcorder("testBackCamcorder720quality", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder720playback", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder720previewLeft", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder720playLeft", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder720previewRight", serialno))    
        suite.addTest(Backcamcorder("testBackCamcorder720playRight", serialno))   

        suite.addTest(Backcamera("testChangetoBackCamera", serialno))  
        suite.addTest(Backcamera("testBackCamera1_0Mpreview", serialno))   
        suite.addTest(Backcamera("testBackCamera1_0M", serialno))   
        suite.addTest(Backcamera("testBackCamera1_0MpreviewLeft", serialno))   
        suite.addTest(Backcamera("testBackCamera1_0MLeft", serialno)) 
        suite.addTest(Backcamera("testBackCamera1_0MpreviewRight", serialno))   
        suite.addTest(Backcamera("testBackCamera1_0MRight", serialno)) 

    elif platform == 'sabresd_6dq':
        suite.addTest(Prepare("preparation", serialno))       #prepare

        suite.addTest(Backcamcorder("testChangetoBackCamcorder", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder1080preview", serialno))       
        suite.addTest(Backcamcorder("testBackCamcorder1080quality", serialno))    
        suite.addTest(Backcamcorder("testBackCamcorder1080playback", serialno))   
        suite.addTest(Backcamcorder("testBackCamcorder720preview", serialno))      
        suite.addTest(Backcamcorder("testBackCamcorder720quality", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder720playback", serialno))     
        suite.addTest(Backcamcorder("testBackCamcorder480preview", serialno))
        suite.addTest(Backcamcorder("testBackCamcorder480quality", serialno)) 
        suite.addTest(Backcamcorder("testBackCamcorder480playback", serialno))   

        suite.addTest(Backcamcorder("testBackCamcorder1080previewLeft", serialno))    
        suite.addTest(Backcamcorder("testBackCamcorder1080playLeft", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder720previewLeft", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder720playLeft", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder480previewLeft", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder480playLeft", serialno))    

        suite.addTest(Backcamcorder("testBackCamcorder1080previewRight", serialno))   
        suite.addTest(Backcamcorder("testBackCamcorder1080playRight", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder720previewRight", serialno))  
        suite.addTest(Backcamcorder("testBackCamcorder720playRight", serialno)) 
        suite.addTest(Backcamcorder("testBackCamcorder480previewRight", serialno)) 
        suite.addTest(Backcamcorder("testBackCamcorder480playRight", serialno))  


        suite.addTest(Backcamera("testChangetoBackCamera", serialno))  
        suite.addTest(Backcamera("testBackCamera5_0Mpreview", serialno))  
        suite.addTest(Backcamera("testBackCamera5_0M", serialno))  
        suite.addTest(Backcamera("testBackCamera0_8Mpreview", serialno))  
        suite.addTest(Backcamera("testBackCamera0_8M", serialno))   
        suite.addTest(Backcamera("testBackCamera0_3Mpreview", serialno)) 
        suite.addTest(Backcamera("testBackCamera0_3M", serialno))  
        suite.addTest(Backcamera("testBackCamera2_1Mpreview", serialno))  
        suite.addTest(Backcamera("testBackCamera2_1M", serialno))   
        suite.addTest(Backcamera("testBackCamera0_9Mpreview", serialno))  
        suite.addTest(Backcamera("testBackCamera0_9M", serialno)) 

        suite.addTest(Backcamera("testBackCamera5_0MpreviewLeft", serialno))  
        suite.addTest(Backcamera("testBackCamera5_0MLeft", serialno))   
        suite.addTest(Backcamera("testBackCamera0_8MpreviewLeft", serialno))  
        suite.addTest(Backcamera("testBackCamera0_8MLeft", serialno))  
        suite.addTest(Backcamera("testBackCamera0_3MpreviewLeft", serialno))   
        suite.addTest(Backcamera("testBackCamera0_3MLeft", serialno))   
        suite.addTest(Backcamera("testBackCamera2_1MpreviewLeft", serialno))   
        suite.addTest(Backcamera("testBackCamera2_1MLeft", serialno))  
        suite.addTest(Backcamera("testBackCamera0_9MpreviewLeft", serialno)) 
        suite.addTest(Backcamera("testBackCamera0_9MLeft", serialno))   

        suite.addTest(Backcamera("testBackCamera5_0MpreviewRight", serialno))  
        suite.addTest(Backcamera("testBackCamera5_0MRight", serialno))  
        suite.addTest(Backcamera("testBackCamera0_8MpreviewRight", serialno))   
        suite.addTest(Backcamera("testBackCamera0_8MRight", serialno))   
        suite.addTest(Backcamera("testBackCamera0_3MpreviewRight", serialno))  
        suite.addTest(Backcamera("testBackCamera0_3MRight", serialno))   
        suite.addTest(Backcamera("testBackCamera2_1MpreviewRight", serialno))  
        suite.addTest(Backcamera("testBackCamera2_1MRight", serialno)) 
        suite.addTest(Backcamera("testBackCamera0_9MpreviewRight", serialno))  
        suite.addTest(Backcamera("testBackCamera0_9MRight", serialno)) 

    
        suite.addTest(Frontcamcorder("testChangetoFrontCamcorder", serialno))      
        suite.addTest(Frontcamcorder("testFrontCamcorder720preview", serialno))      
        suite.addTest(Frontcamcorder("testFrontCamcorder720quality", serialno))     
        suite.addTest(Frontcamcorder("testFrontCamcorder720playback", serialno))     
        suite.addTest(Frontcamcorder("testFrontCamcorder480preview", serialno))    
        suite.addTest(Frontcamcorder("testFrontCamcorder480quality", serialno))    
        suite.addTest(Frontcamcorder("testFrontCamcorder480playback", serialno))      

        suite.addTest(Frontcamcorder("testFrontCamcorder720previewLeft", serialno))   
        suite.addTest(Frontcamcorder("testFrontCamcorder720playLeft", serialno))   
        suite.addTest(Frontcamcorder("testFrontCamcorder480previewLeft", serialno))  
        suite.addTest(Frontcamcorder("testFrontCamcorder480playLeft", serialno))   

        suite.addTest(Frontcamcorder("testFrontCamcorder720previewRight", serialno))    
        suite.addTest(Frontcamcorder("testFrontCamcorder720playRight", serialno))   
        suite.addTest(Frontcamcorder("testFrontCamcorder480previewRight", serialno))  
        suite.addTest(Frontcamcorder("testFrontCamcorder480playRight", serialno))  

        suite.addTest(Frontcamera("testChangetoFrontCamera", serialno))      
        suite.addTest(Frontcamera("testFrontCamera5_0Mpreview", serialno))     
        suite.addTest(Frontcamera("testFrontCamera5_0M", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_8Mpreview", serialno))   
        suite.addTest(Frontcamera("testFrontCamera0_8M", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_3Mpreview", serialno))
        suite.addTest(Frontcamera("testFrontCamera0_3M", serialno))   
        suite.addTest(Frontcamera("testFrontCamera2_1Mpreview", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera2_1M", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_9Mpreview", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera0_9M", serialno))  

        suite.addTest(Frontcamera("testFrontCamera5_0MpreviewLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera5_0MLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_8MpreviewLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_8MLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_3MpreviewLeft", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera0_3MLeft", serialno))   
        suite.addTest(Frontcamera("testFrontCamera2_1MpreviewLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera2_1MLeft", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_9MpreviewLeft", serialno))   
        suite.addTest(Frontcamera("testFrontCamera0_9MLeft", serialno)) 

        suite.addTest(Frontcamera("testFrontCamera5_0MpreviewRight", serialno))  
        suite.addTest(Frontcamera("testFrontCamera5_0MRight", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_8MpreviewRight", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera0_8MRight", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_3MpreviewRight", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera0_3MRight", serialno))  
        suite.addTest(Frontcamera("testFrontCamera2_1MpreviewRight", serialno)) 
        suite.addTest(Frontcamera("testFrontCamera2_1MRight", serialno))  
        suite.addTest(Frontcamera("testFrontCamera0_9MpreviewRight", serialno))   
        suite.addTest(Frontcamera("testFrontCamera0_9MRight", serialno))  


    if os.getenv("debug") == None:
        filename = file_name + '.html'
        fp = open(filename, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp,title=u"Camera Test Result",description=u"Camera Test",verbosity=2)
        runner.run(suite)
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
        
