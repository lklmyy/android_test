import sys
import os
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_path+'/../core')
sys.path.append(file_path+'/../utils')
import device  # noqa
import unittest  # noqa
import HTMLTestRunner  # noqa
from encode import EncoderTest  # noqa

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path+'/../result/'+file_name):
    os.makedirs(file_path+'/../result/'+file_name)
os.chdir(file_path+'/../result/'+file_name)

if __name__ == "__main__":
    suite = unittest.TestSuite()
    if os.getenv("sn") is None:
        print('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print("no sn exported, Run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)

    platform = os.popen(u'adb -s ' + serialno +
                        ' shell getprop ro.product.name').read().strip()
    if platform == 'mek_8q':
        suite.addTest(EncoderTest("testMP4_H264AAC", serialno))
        # only support h264,aac,mp4 no need to do prepare
    elif platform == 'sabresd_6dq':
        suite.addTest(EncoderTest("prepare", serialno))       # prepare
        suite.addTest(EncoderTest("testMP4_H264AAC", serialno))
        suite.addTest(EncoderTest("test3GP_H264AAC", serialno))
        suite.addTest(EncoderTest("testMP4_H264AMRNB", serialno))
        suite.addTest(EncoderTest("test3GP_H264AMRNB", serialno))
        suite.addTest(EncoderTest("testMP4_H264AMRWB", serialno))
        suite.addTest(EncoderTest("test3GP_H264AMRWB", serialno))
        suite.addTest(EncoderTest("testMP4_H263AAC", serialno))
        suite.addTest(EncoderTest("test3GP_H263AAC", serialno))
        suite.addTest(EncoderTest("testMP4_H263AMRNB", serialno))
        suite.addTest(EncoderTest("test3GP_H263AMRNB", serialno))
        suite.addTest(EncoderTest("testMP4_H263AMRWB", serialno))
        suite.addTest(EncoderTest("test3GP_H263AMRWB", serialno))
        suite.addTest(EncoderTest("testMP4_MPEG4AAC", serialno))
        suite.addTest(EncoderTest("test3GP_MPEG4AAC", serialno))
        suite.addTest(EncoderTest("testMP4_MPEG4AMRNB", serialno))
        suite.addTest(EncoderTest("test3GP_MPEG4AMRNB", serialno))
        suite.addTest(EncoderTest("testMP4_MPEG4AMRWB", serialno))
        suite.addTest(EncoderTest("test3GP_MPEG4AMRWB", serialno))
        suite.addTest(EncoderTest("restore", serialno))

    if os.getenv("debug") is None:
        filename = file_name + '.html'
        fp = open(filename, 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                                               title=u"Encoder Test Result",
                                               description=u"Encoder Test",
                                               verbosity=2)
        runner.run(suite)
    else:
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
