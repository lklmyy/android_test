#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import sys
import os
file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_path + '/../core')
sys.path.append(file_path + '/../utils')
import unittest # noqa
import HTMLTestRunner # noqa
import device # noqa
from wifibt import WIFI # noqa
from wifibt import BT # noqa
from wifibt import AirPlane # noqa

file_name = os.path.basename(__file__).split('.', 1)[0]
if not os.path.exists(file_path + '/../result/' + file_name):
    os.makedirs(file_path + '/../result/' + file_name)
os.chdir(file_path + '/../result/' + file_name)

if __name__ == '__main__':
    suite = unittest.TestSuite()

    if os.getenv("sn") is None:
        print('connected devices:', device.get_devices())
        serialno = device.get_devices()[0]
        print("no sn exported, Run on default device:", serialno)
    else:
        serialno = os.getenv("sn")
        print("run on device:", serialno)

    suite.addTest(WIFI("reset", serialno))
    suite.addTest(WIFI("wifiSwitch", serialno))
    suite.addTest(WIFI("wifiConnect", serialno))
    suite.addTest(WIFI("wifiSwitch2", serialno))
    suite.addTest(WIFI("wifiDisconnect", serialno))
    suite.addTest(WIFI("wifiConnect2", serialno))

    suite.addTest(BT("turnOn", serialno))
    suite.addTest(BT("changeBtName", serialno))
    suite.addTest(BT("btSwitch", serialno))

    suite.addTest(AirPlane("airplaneON", serialno))
    suite.addTest(AirPlane("airplaneOFF", serialno))

    if os.getenv("debug") is None:
        fp = open(file_name + '.html', 'wb')
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=u"Test Result", description=u"Detail Descrpiton", verbosity=2)
        runner.run(suite)
    else:
        print("debug mode")
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
