Precondition:
Before using the scripts we developed, please ensure you have installed below tools:
1. Python3.x
2. uiautomator
3. pyusb
4. unittest

There are 5 folders in uiautomator 
1. core
    1) All the test cases we developed were putted in this folder;
    2) We organized these cases according to modules listed in XT app case pool, e.g: WiFi, GUI, Camera, etc;
2. test
    1) We created several configure script file in this folder so that user can directly using the cases;
    2) Take testCalculator.py script as an example, you can see this script only include one case: testMonkey which belong to Calculator class;
    3) You can direct run this script: python3 testCalculator.py
       Then a html test report will produce after finish testing
    4) You also can follow the rule to add your own test case suits
3. Utils
    1) This folder contains some common function;
4. images
    1) This folder contains fastboot flash script;
    2) We should copy needed images to this folder before running fastboot upgrade test
5. config
    1) This folder contains yaml file for multi-platforms support

There are 2 environment variable defined
1. debug
    1) For decide output result to HTML file or print on console;
    2) Default value is "None" and test result will be output to HTML file;
    
2. sn
    1) Device serial number, for decide which android device the test will be executed on;
    2) Default value is "None", the first android device serial number will be selected.

Please contact me by below mail address if you met any issue while using thest scripts:
	Kailiang Li(13120551826@163.com)