#!/bin/bash
set -e

adb reboot bootloader

fastboot oem unlock
fastboot flash bootloader0 u-boot-imx8qm.imx
fastboot reboot bootloader
fastboot flash gpt partition-table.img
fastboot flash boot_a boot-imx8qm.img
fastboot flash boot_b boot-imx8qm.img
fastboot flash vbmeta_a vbmeta-imx8qm.img
fastboot flash vbmeta_b vbmeta-imx8qm.img
fastboot flash system_a system.img
fastboot flash system_b system.img
fastboot flash vendor_a vendor.img
fastboot flash vendor_b vendor.img
fastboot reboot

echo "Successfully flashed your device."

