#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# ---------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP
# Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Kailiang Li
# Created Date: 3/14/2018
# Updated Date: 6/28/2018
#
# Description: Android Settings test script
# ---------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb, PIL ,pyyaml and uiautomator have been
#    installed on the Ubuntu OS PC you used;
# 2. Result should only be pass or fail if testing on supported boards
#    according to the HTML file;
# 3. For more detail information, please refer to the official documents :
#      https://docs.python.org/3/library/index.html;
# 4. Please check the screen shots after run pass for the specified
#    script: "testColors" in testResult folder.
# 5. Factory reset the board before executing the scripts
# ---------------------------------------------------------------------------

import math
import time
import operator
import unittest
from PIL import Image
from shutil import copy
from functools import reduce
from uiautomator import Device
from yaml import load

import os
import sys
sys.path.append('../core')
sys.path.append('../utils')
sys.path.append('../resource/image')
config_path = os.path.join(os.path.abspath(os.path.dirname(__file__)) +
                           '/../config', 'settings.yaml')
from setup import set_up, tear_down
from clear import Clear
from common import Common


class SettingsCommon(Common):
    """
     SettingsCommon contains some common functions suitable for setting module
    """
    def __init__(self, d, serialno):
        super(SettingsCommon, self).__init__(d, serialno)

        """common widget information constant"""

        self.ANDROID_WIDGET_FRAME = "android:id/widget_frame"
        self.ANDROID_WIDGET_SWITCH = "android.widget.Switch"
        self.ANDROID_ICON = "android:id/icon"
        self.SYSTEMUI_SETTINGS_BUTTON_ID = ("com.android.systemui:id/settings"
                                            "_button")
        self.SYSTEMUI_USER_NAME_ID = "com.android.systemui:id/user_name"
        self.SYSTEMUI_EXPAND_INDICATOR_ID = ("com.android.systemui:id/expand"
                                             "_indicator")
        self.ANDROID_WIDGET_RADIOBUTTON = "android.widget.RadioButton"
        self.ANDROID_LARGER = "com.android.settings:id/larger"
        self.ANDROID_SMALLER = "com.android.settings:id/smaller"
        self.ANDROID_SETTINGS_BUTTON = ("com.android.settings:id/settings"
                                        "_button")
        self.DESKCLOCK_CLOCK = "com.android.deskclock:id/main_clock"
        self.ANDROID_WIDGET_RADIAL = ("android.widget.RadialTimePickerView$"
                                      "RadialPickerTouchHelper")

    def compare_image(self, src, dst):
        """Using color histogram compare whether the two pictures (src and
           dst)are the same. If they are same the result is zero,otherwise
           the greater the difference, the greater the return value.

           @:param src: Source image
           @:param dst: Destination image

           Note:
               If the method is called, ensure that the PIL lib has been
               installed .
        """
        srcImage = Image.open(src)
        dstImage = Image.open(dst)
        srcHist = srcImage.histogram()
        dstHist = dstImage.histogram()
        result = math.sqrt(reduce(operator.add,
                                  list(map(lambda a, b: (a - b) ** 2,
                                           srcHist, dstHist))) / len(srcHist))
        return result

    def delete_files(self, path, string):
        """ Delete all specified type of files under the current directory
            and its subdirectories.
            Usage:
            @:param path: refers to the root directory to which files need
                          to be deleted.
            @:param string: file type that needs to be deleted

            Example:
            string = ".png"
            strlist = (".png", ".jpg", ".bmp")
        """
        for root, dirs, files in os.walk(path):
            for name in files:
                if name.endswith(string):
                    os.remove(os.path.join(root, name))
                    print("Delete File: " + os.path.join(root, name))

    def view_group_quick_setting(self, key):
        """check if the button has already opened ,if not ,open it. Supported
           text name includes:
            Usage eg:
               text=dict[key]
        """
        quickset = {"Ai": "Airplane mode", "Lan": "Landscape",
                    "Au": "Auto-rotate"}
        self.d.swipe(0, 0, 0, 400, 100)
        time.sleep(2)
        if self.d(resourceId=self.SYSTEMUI_EXPAND_INDICATOR_ID).exists:
            self.d(resourceId=self.SYSTEMUI_EXPAND_INDICATOR_ID).click.wait()

        if quickset[key] == "Airplane mode":
            if self.d(className=self.ANDROID_WIDGET_SWITCH,
                      text="Off").child(text=quickset[key]).exists:
                self.d(className=self.ANDROID_WIDGET_SWITCH,
                       text="Off").child(text=quickset[key]).click.wait()
            else:
                print("this button has already been opened")

        elif quickset[key] == "Landscape":
            if self.d(text="Landscape").exists:
                print("the Auto-rotate mode has already opened")
            else:
                self.d(text="Auto-rotate").click()

        elif quickset[key] == "Auto-rotate":
            if self.d(text="Auto-rotate").exists:
                print("the Auto-rotate mode has already opened")
            else:
                self.d(text="Landscape").click()

        self.d(className="android.widget.LinearLayout").child(
            resourceId=self.SYSTEMUI_EXPAND_INDICATOR_ID).click.wait()
        self.d.press.back()
        time.sleep(2)

    def rotate_screen(self, value):
        """
        orienting the devie to left/right or natural.
        left/l:       rotation=90 , displayRotation=1
        right/r:      rotation=270, displayRotation=3
        natural/n:    rotation=0  , displayRotation=0
        upsidedown/u: rotation=180, displayRotation=2
        """
        try:
            self.d.press.back()
            self.d.orientation = value
            time.sleep(3)
        except Exception as e:
            print(e)
            raise

    def display(self):
        self.d(text="Display").click.wait()

    def open_screen_saver(self):
        self.display()
        if self.d(resourceId=self.ANDROID_ICON).exists:
            self.d(resourceId=self.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Screen saver").click.wait()

    def open_date_time(self):
        self.display()
        self.d.press.back()
        self.d(scrollable=True).scroll.to(text="System")
        self.d(text="System").click.wait()
        self.d(text="Date & time").click.wait()

    def get_yaml_data(self):
        with open(config_path, 'rb') as yamlFile:
            cont = yamlFile.read()
        yamlFile = load(cont)
        return yamlFile

    def get_android_version_model(self):
        """find current android version

           Note: This method needs to be updated when a new version is
                 published. Just enlarge the version below

        """
        version = ["8.1.0", "8.0.0"]
        self.d(scrollable=True).scroll.to(text="System")
        self.d(text="System").click.wait()
        self.d(text="About tablet").click.wait()
        for i in range(len(version)):
            if self.d(text=version[i]).exists:
                return version[i]
        self.d.press.home()
        # pass
        # version = ["8.1.0", "8.0.0"]
        # model = ["EVK_MX6SL", "MEK-MX8Q", "SABRESD-6DQ"]
        # self.d(scrollable=True).scroll.to(text="System")
        # self.d(text="System").click.wait()
        # self.d(text="About tablet").click.wait()
        #
        # for i in range(len(version)):
        #     if self.d(text=version[i]).exists:
        #         currentversion = version[i]
        #     else:
        #         raise Exception("No correspondversion exist,Please check "
        #                         "if current version has been added")
        #     for j in range(len(model)):
        #         if self.d(text=model[j]).exists:
        #             currentmodel = model[j]
        #             return currentversion, currentmodel
        #         else:
        #             raise Exception("No corresponding model exist,Please"
        #                            " check if current model has been added")
        # self.d.press.home()


class AndroidSettings(unittest.TestCase):

    def __init__(self, name, serialno):
        """Create an instance of the class that will use the named test
           method when executed. Raises a ValueError if the instance does
           not have a method with the specified name.
        """
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.settingscommon = SettingsCommon(self.d, self.serialno)
        self.settingscommon.d = self.d
        self.settingscommon.serialno = self.serialno
        self.clear = Clear(self.settingscommon.d,
                           self.settingscommon.serialno)
        self.yamldata = self.settingscommon.get_yaml_data()
        self.platform = os.popen(
            u'adb -s ' + self.serialno + ' shell getprop ro.product.name')\
            .read().strip()

    def setUp(self):
        """1. Set up the device and enter the Settings UI.
           2. Hook method for setting up the test fixture before exercising
              it.
        """
        set_up(self.platform, self.d, self.serialno)
        self.d(text="Settings").click.wait()

    def tearDown(self):
        """
          Hook method for deconstructing the test fixture after testing it.
        """
        try:
            tear_down(self.platform, self.d, self.serialno)
        except Exception as e:
            print(e)

# ===========================================================================
# test System Brightness
# ===========================================================================

    def testBrightnessLevel(self):
        """
           Light sensor should can work from low to high,when slide the bar to
           right,d(text="100%").exists would return true.
               Note:the button of Adaptive brightness should be closed while
                    running this case
        """
        self.settingscommon.display()
        if self.d(text="Adaptive brightness").right(
                resourceId=self.settingscommon.ANDROID_WIDGET_FRAME).child(
                text="ON").exists:
            self.d(text="Adaptive brightness").right(
                resourceId=self.settingscommon.ANDROID_WIDGET_FRAME).child(
                text="ON").click()
            time.sleep(2)
        self.d(text="Brightness level").click.wait()
        self.d(resourceId="com.android.systemui:id/slider").swipe("right",
                                                                  steps=100)
        self.d.press.back()
        time.sleep(2)
        if self.d(text="100%").exists:
            result = True
        else:
            result = False
        expected_result = True
        self.clear.clearAll()
        self.assertEqual(result, expected_result)

    def testAdaptiveBrightness(self):
        """
        It can auto change the backlight of LVDS by the environment light
        strength
        """
        self.settingscommon.display()
        if self.d(text="Adaptive brightness").right(
                resourceId=self.settingscommon.ANDROID_WIDGET_FRAME).child(
                text="ON").exists:
            self.d(text="Adaptive brightness").right(
                resourceId=self.settingscommon.ANDROID_WIDGET_FRAME).child(
                text="ON").click()
        time.sleep(2)
        putbrightvalue = os.system('adb -s ' +self.serialno +' shell settings put system screen_brightness 255')
        time.sleep(2)
        self.d(text="Adaptive brightness").right(
            resourceId=self.settingscommon.ANDROID_WIDGET_FRAME).child(
            text="OFF").click()
        time.sleep(2)
        getbrightvalue = os.system('adb -s ' + self.serialno + ' shell settings get system screen_brightness')
        if putbrightvalue != getbrightvalue:
            result = True
        else:
            result = False
        expected_result = True
        self.clear.clearAll()
        self.assertEqual(result, expected_result)

# ===========================================================================
# test display size
# ===========================================================================

    def testFontSmallButtonA(self):
        """
        When set to the smallest, the small A is grey out and can't change
        again
        """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            self.d(text="Font size").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
            time.sleep(2)
            if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                      index="0", enabled="true").exists:
                self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                       index="0", enabled="true").click.wait()
                if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                          index="0", enabled="false").exists and \
                        self.d(resourceId=self.settingscommon.ANDROID_LARGER,
                               index="1", enabled="true").exists:
                    result = True
                else:
                    result = False
                self.assertTrue(result)
            else:
                print("the A Button is unavailable or not exist")
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
        else:
            print("the Font size is not exist")
        self.clear.clearAll()

    def testFontBigButtonA(self):
        """
        When set to the largest, the big A is grey out and can't change
        again
        """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            self.d(text="Font size").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="2").click.wait()
            time.sleep(2)
            if self.d(resourceId=self.settingscommon.ANDROID_LARGER,
                      index="1", enabled="true").exists:
                self.d(resourceId=self.settingscommon.ANDROID_LARGER,
                       index="1", enabled="true").click.wait()
                if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                          index="0", enabled="true").exists and self.d(
                    resourceId=self.settingscommon.ANDROID_LARGER, index="1",
                    enabled="false"
                ).exists:
                    result = True
                else:
                    result = False
                self.assertTrue(result)
            else:
                print("the A Button is unavailable or not exist")
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
        else:
            print("the Font size is not exist")
        self.clear.clearAll()

# ===========================================================================
# test Display button - and +
# ===========================================================================

    def testDisplayMinusButton(self):
        """
        When set to the smallest , the -  button is grey out and can't
        change again
        """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            self.d(text="Display size").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
            time.sleep(2)
            if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                      index="0", enabled="true").exists:
                self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                       index="0", enabled="true").click.wait()
                if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                          index="0", enabled="false").exists and self.d(
                    resourceId=self.settingscommon.ANDROID_LARGER,
                    index="1",
                    enabled="true"
                ).exists:
                    result = True
                else:
                    result = False
                self.assertTrue(result)
            else:
                print("the - Button is unavailable or not exist")
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
        else:
            print("the Display size is not exist")
        self.clear.clearAll()

    def testDisplayPlusButton(self):
        """
         When set to the largest, the + button is grey out and can't change
         again
         """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            self.d(text="Display size").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="3").click.wait()
            time.sleep(2)
            if self.d(resourceId=self.settingscommon.ANDROID_LARGER,
                      index="1", enabled="true").exists:
                self.d(resourceId=self.settingscommon.ANDROID_LARGER,
                       index="1", enabled="true").click.wait()
                if self.d(resourceId=self.settingscommon.ANDROID_SMALLER,
                          index="0", enabled="true").exists and self.d(
                    resourceId=self.settingscommon.ANDROID_LARGER,
                    index="1",
                    enabled="false"
                ).exists:
                    result = True
                else:
                    result = False
                self.assertTrue(result)
            else:
                print("the + Button is unavailable or not exist")
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
        else:
            print("the Display size is not exist")
        self.clear.clearAll()

# ===========================================================================
# test font size
# Note: This module include three scripts, when last script executed ,The
#       display will be restored to the default state
# ===========================================================================

    def testFontSizeSmall(self):
        """ Set from small A to big A font from the Bar or just press A
            button.
            Check the left preview screen font change accordingly.
        """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="0").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("srcFontSize.png")
        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("dstFontSize.png")

        result = self.settingscommon.compare_image("srcFontSize.png",
                                                   "dstFontSize.png")
        print(result)
        if result <= 10:
            except_result = False
        else:
            except_result = True
        self.clear.clearAll()
        self.assertTrue(except_result)

    def testFontSizeDefault(self):
        """ Set from small A to big A font from the Bar or just press A
            button.
            Check the left preview screen font change accordingly.
        """
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="0").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("srcFontSize.png")

        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="2").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("dstFontSize.png")

        result = self.settingscommon.compare_image("srcFontSize.png",
                                                   "dstFontSize.png")
        print(result)
        if result <= 10:
            except_result = False
        else:
            except_result = True
        self.clear.clearAll()
        self.assertTrue(except_result)

    def testFontSizeLarge(self):
        """ Set from small A to big A font from the Bar or just press A
            button.
            Check the left preview screen font change accordingly.
        """
        self.settingscommon.display()
        self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="0").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("srcFontSize.png")

        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="3").click.wait()
        self.d.press.back()
        time.sleep(2)
        self.d.screenshot("dstFontSize.png")

        result = self.settingscommon.compare_image("srcFontSize.png",
                                                   "dstFontSize.png")
        print(result)
        if result <= 10:
            except_result = False
        else:
            except_result = True
        self.assertTrue(except_result)

        # modify Font size module to default settings and delete cached data
        self.d(text="Font size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.press.home()
        self.clear.clearAll()
        file = os.path.abspath(os.path.dirname(__file__))
        string = ".png"
        self.settingscommon.delete_files(file, string)

# ===========================================================================
# test display size
# ===========================================================================

    def testDisplaySizeSmall(self):
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Display size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.screenshot("srcDisplayChatSize.png")
        try:
            # compare Chat window
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="0").click.wait()
            time.sleep(2)
            self.d.screenshot("dstDisplayChatSize.png")
            result = self.settingscommon.compare_image(
                "srcDisplayChatSize.png", "dstDisplayChatSize.png")
            print(result)
            if result <= 10:
                except_result = False
            else:
                except_result = True
            self.clear.clearAll()
            self.assertTrue(except_result)
        except AssertionError as e:
            self.clear.clearAll()
            print("compare Chat window：", e)
            raise

    def testDisplaySizeLarge(self):
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Display size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.screenshot("srcDisplayChatSize.png")
        try:
            # compare Chat window
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="2").click.wait()
            time.sleep(2)
            self.d.screenshot("dstDisplayChatSize.png")
            result = self.settingscommon.compare_image(
                "srcDisplayChatSize.png", "dstDisplayChatSize.png")
            print(result)
            if result <= 10:
                except_result = False
            else:
                except_result = True
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="1").click.wait()
            self.clear.clearAll()
            self.assertTrue(except_result)
        except AssertionError as e:
            self.clear.clearAll()
            print("compare Chat window：", e)
            raise

    def testDisplaySizeLarger(self):
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Display size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.screenshot("srcDisplayChatSize.png")
        try:
            # compare Chat window
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="3").click.wait()
            time.sleep(2)
            self.d.screenshot("dstDisplayChatSize.png")
            result = self.settingscommon.compare_image(
                "srcDisplayChatSize.png", "dstDisplayChatSize.png")
            print(result)
            if result <= 10:
                except_result = False
            else:
                except_result = True
            self.assertTrue(except_result)
        except AssertionError as e:
            print("compare Chat window：", e)
            raise

        # modify Font size module to default settings and delete cached data
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.press.home()
        self.clear.clearAll()
        file = os.path.abspath(os.path.dirname(__file__))
        string = ".png"
        self.settingscommon.delete_files(file, string)

    def testDisplaySizeLargest(self):
        self.settingscommon.display()
        if self.d(resourceId=self.settingscommon.ANDROID_ICON).exists:
            self.d(resourceId=self.settingscommon.ANDROID_ICON).click.wait()
            time.sleep(1)
        self.d(text="Display size").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.screenshot("srcDisplayChatSize.png")
        try:
            # compare Chat window
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
                   index="4").click.wait()
            time.sleep(2)
            self.d.screenshot("dstDisplayChatSize.png")
            result = self.settingscommon.compare_image(
                "srcDisplayChatSize.png", "dstDisplayChatSize.png")
            print(result)
            if result <= 10:
                except_result = False
            else:
                except_result = True
            self.assertTrue(except_result)
        except AssertionError as e:
            print("compare Chat window：", e)
            raise
        time.sleep(2)

        # modify Font size module to default settings and delete cached data
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIOBUTTON,
               index="1").click.wait()
        time.sleep(2)
        self.d.press.home()
        self.clear.clearAll()
        file = os.path.abspath(os.path.dirname(__file__))
        string = ".png"
        self.settingscommon.delete_files(file, string)

# ===========================================================================
# test Screen saver
# Note:The following four scripts are currently available only to Android8.1
#     but not applicable to Android8.0
# model issue: While executing the following step: Settings->Display->Screen
#     saver->Current screen saver->Clock
#     there will be a widget which resourceId= self.ANDROID_SETTINGS_BUTTON ,
#     index="0"  this widget is not available and  an information "Clock has
#     stopped" will be popped out in Android8.0
# ===========================================================================

    def testClockAnalog(self):
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Clock").click.wait()
        if self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                  index="0").exists:
            self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                   index="0").click.wait()
        self.d(text="Style").click.wait()
        self.d(text="Analog").click.wait()
        self.d.press.back()
        self.d(text="When to start").click.wait()
        self.d(text="While charging").click.wait()
        self.d(text="START NOW").click.wait()
        if self.d(text="GOT IT").wait.exists():
            self.d(text="GOT IT").click.wait()
        time.sleep(4)
        if self.d(resourceId=self.settingscommon.DESKCLOCK_CLOCK).exists:
            result = True
        else:
            result = False
        self.assertTrue(result)
        self.d.screen.off()
        time.sleep(4)
        self.d.screen.on()
        self.clear.clearAll()

    def testClockAnalogNightMode(self):
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Clock").click.wait()
        if self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                  index="0").exists:
            self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                   index="0").click.wait()
        self.d(text="Style").click.wait()
        self.d(text="Analog").click.wait()
        if self.d(text="Night mode").right(checked="false").exists:
            self.d(text="Night mode").click.wait()
        self.d.press.back()
        self.d(text="When to start").click.wait()
        self.d(text="While charging").click.wait()
        self.d(text="START NOW").click.wait()
        if self.d(text="GOT IT").wait.exists():
            self.d(text="GOT IT").click.wait()
        time.sleep(4)
        if self.d(className="android.widget.ImageView").exists:
            result = True
        else:
            result = False
        self.assertTrue(result)
        self.clear.clearAll()

    def testClockDigital(self):
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Clock").click.wait()
        if self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                  index="0").exists:
            self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                   index="0").click.wait()
        self.d(text="Style").click.wait()
        self.d(text="Digital").click.wait()
        time.sleep(2)
        self.d.press.back()
        self.d(text="When to start").click.wait()
        self.d(text="While charging").click.wait()
        self.d(text="START NOW").click.wait()
        if self.d(text="GOT IT").wait.exists():
            self.d(text="GOT IT").click.wait()
        time.sleep(4)
        if self.d(resourceId="com.android.deskclock:id/digital_clock").exists:
            result = True
        else:
            result = False
        self.clear.clearAll()
        self.assertTrue(result)

    def testClockDigitalNightMode(self):
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Clock").click.wait()
        if self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                  index="0").exists:
            self.d(resourceId=self.settingscommon.ANDROID_SETTINGS_BUTTON,
                   index="0").click.wait()
        self.d(text="Style").click.wait()
        self.d(text="Digital").click.wait()
        self.d(text="Night mode").click.wait()
        self.d.press.back()
        self.d(text="When to start").click.wait()
        self.d(text="While charging").click.wait()
        self.d(text="START NOW").click.wait()
        if self.d(text="GOT IT").wait.exists():
            self.d(text="GOT IT").click.wait()
        time.sleep(4)
        if self.d(resourceId="com.android.deskclock:id/digital_clock").exists:
            result = True
        else:
            result = False
        self.clear.clearAll()
        self.assertTrue(result)

    def testColors(self):
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Colors").click.wait()
        self.d(text="START NOW").click.wait()
        time.sleep(4)
        self.d.screenshot("screenSaverColors.png")
        self.d.press.home()
        time.sleep(2)
        self.clear.clearAll()
        file = os.path.abspath(os.path.dirname(__file__))
        string = ".png"
        self.settingscommon.delete_files(file, string)

# ===========================================================================
# test developer options
# ===========================================================================

    def testDeveloperOptions(self):
        self.settingscommon.display()
        self.d.press.back()
        self.d(scrollable=True).scroll.to(text="System")
        self.d(text="System").click.wait()
        self.d(text="About tablet").click.wait()
        time.sleep(2)
        for i in range(7):
            self.d(text="Build number").click()
        self.d.press.back()
        try:
            self.d(text="Developer options").click.wait()
            self.d(scrollable=True).scroll.to(text="USB debugging")
            if self.d(text="USB debugging").right(text="ON"):
                print("Developer mode has already opened ")
        except Exception as e:
            print("Developer mode open fail !")
            print(e)
        print("Developer Options case run successfully")

# ===========================================================================
# test System UI Tuner:Check utility of System UI Tuner
# ==========================================================================

    def testUITuner(self):
        """test if System UI Tuner appears
           Note: something wrong with the long_click()
        """
        try:
            self.d.swipe(0, 0, 0, 400, 100)
            time.sleep(2)
            self.d(
                resourceId=self.settingscommon.SYSTEMUI_SETTINGS_BUTTON_ID)\
                .long_click()
            time.sleep(4)
            if self.d(text="CANCEL").exists:
                self.d(text="CANCEL").click.wait()
            time.sleep(2)
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            if self.d(text="System UI Tuner").exists:
                self.assertTrue(True)
            else:
                self.assertTrue(True)
                pass
            self.d.swipe(0, 0, 0, 400, 100)
            time.sleep(2)
            self.d(
                resourceId=self.settingscommon.SYSTEMUI_SETTINGS_BUTTON_ID)\
                .long_click()  # in 8.1.0
            time.sleep(4)
            pass
            self.d(text="REMOVE").click.wait()
        except Exception as e:
            print(e)
            raise

    def testStatusBarAirplaneMode(self):
        """Status bar, disable some of them, check status bar when launch
           corresponding app. Such as, disable status
           of Airplane mode, then no plane icon if enable Airplane mode;
           or Auto-rotate screen etc
        """
        self.clear.clearAll()
        self.settingscommon.view_group_quick_setting("Ai")
        set_up(self.platform, self.d, self.serialno)
        self.d(scrollable=True).scroll.to(text="System")
        self.d(text="System").click.wait()
        if not self.d(text="System UI Tuner").exists:
            self.d.swipe(0, 0, 0, 400, 100)
            self.d(
                resourceId=self.settingscommon.SYSTEMUI_SETTINGS_BUTTON_ID)\
                .long_click()
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
        if self.d(text="System UI Tuner").exists:
            self.d(text="System UI Tuner").click.wait()
            time.sleep(3)
            if self.d(text="GOT IT").exists:
                self.d(text="GOT IT").click.wait()
            time.sleep(4)
            self.d.screenshot("systemuitunner.png")

            # check Airplane mode status
            time.sleep(2)
            self.d(text="Status bar").click.wait()
            self.d(scrollable=True).scroll.to(text="Airplane mode")
            self.d(text="Airplane mode").click.wait()
            self.d.press.back()
            time.sleep(2)
            self.d.screenshot("systemuitunner1.png")
            result = self.settingscommon.compare_image("systemuitunner.png",
                                                       "systemuitunner1.png")
            print(result)
            if result == 0:
                except_result = False
            else:
                except_result = True
            self.d.swipe(0, 0, 0, 400, 100)
            if self.d(
                resourceId=self.settingscommon.SYSTEMUI_EXPAND_INDICATOR_ID)\
                    .exists:
                temp = self.settingscommon.SYSTEMUI_EXPAND_INDICATOR_ID
                self.d(resourceId=temp).click.wait()
            if self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                      description="Airplane mode").exists:
                self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                       description="Airplane mode").click.wait()
            self.assertTrue(except_result)
        else:
            self.assertTrue(True)
            pass
            # SettingsException("something wrong with long_click()")
        time.sleep(2)
        self.clear.clearAll()
        file = os.path.abspath(os.path.dirname(__file__))
        string = ".png"
        self.settingscommon.delete_files(file, string)

# ===========================================================================
# test All kinds of language display ok
# ===========================================================================

    def testLanguage(self):
        """
        if the version changes,additional information is needed in
        conditional judgement.
        """
        try:
            # version = self.settingscommon.get_android_version_model()
            SETTINGS_DRAGHANDLE = "com.android.settings:id/dragHandle"
            time.sleep(2)
            self.clear.clearAll()
            set_up(self.platform, self.d, self.serialno)
            self.d(scrollable=True).scroll.to(text="System")
            time.sleep(2)
            self.d(text="System").click.wait()
            self.d(textContains="Languages").click.wait()
            """
            if version == "8.1.0":
                self.d(text="Languages & input").click.wait()

            elif version == "8.0.0":
                self.d(text="Languages, input & gestures").click.wait()

            else:
               # Add new version here if version changed
                pass
                raise Exception("The version has been updated, please add new
                version information")
            """
            # add a language
            self.d(text="Languages").click.wait()

            def removeLanguage():
                # remove a language
                self.d(className="android.widget.ImageButton",
                       description="More options").click.wait()
                self.d(text="Remove").click.wait()
                time.sleep(2)
                self.d(textContains="日本").click.wait()
                self.d(description="Remove").click.wait()
                time.sleep(2)
                self.d(text="OK").click.wait()

            if self.d(textContains="日本").exists:
                removeLanguage()
            self.d(text="Add a language").click.wait()
            self.d(scrollable=True).scroll.to(textContains="日本")
            time.sleep(4)
            self.d(textContains="日本").click.wait()
            try:
                self.assertTrue(self.d(textContains="日本").exists)
            except Exception as e:
                print(e)
                print("adding a language fails")

            # top move a language
            self.d(textContains="日本").right(
                resourceId=SETTINGS_DRAGHANDLE).drag.to(0, 0)
            time.sleep(3)
            try:
                self.assertTrue(self.d(textContains="日本").left(
                    text="1").exists)
                time.sleep(2)
                self.assertTrue(self.d(text="言語の設定").exists)
            except Exception as e:
                print(e)
                print("top move a language fails")
            time.sleep(2)
            if self.d(text="English (United States)").right(
                    resourceId=SETTINGS_DRAGHANDLE).exists:
                self.d(text="English (United States)").right(
                    resourceId=SETTINGS_DRAGHANDLE).drag.to(0, 0)
            removeLanguage()
            try:
                self.assertFalse(self.d(textContains="日本").exists)
            except Exception as e:
                print(e)
                print("remove a language fails")
            time.sleep(2)
            self.clear.clearAll()
        except Exception as e:
            print(e)
            self.clear.clearAll()
            self.assertFalse(True)

# ===========================================================================
#  test Date & Time
#     SRS Items:
#       GEN_DAT_00005   User can manually change Date/Time from UI
#       GEN_DAT_00010	Date/Time can be set to automatically using network
#                       provided value
#       GEN_DAT_00011	Time zone can be set to automatically using network
#                       provided value
#       GEN_DAT_00015	Date/Time should be kept even when the device is
#                       powered off
#       GEN_DAT_00020	User can add/change/delete alarm for specified
#                       date/time/repeat pattern and alerting methods
#                       (ring tone/vibrate)
#       GEN_DAT_00025	User can set multiple alarms
#       GEN_DAT_00030	User set alarm should wake up device in sleep state
#       GEN_DAT_00035	User set alarm should automatically power up the
#                       device to alarm if it's powered off
# ===========================================================================

    def testSetDate(self):
        try:
            self.settingscommon.open_date_time()
            if self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                      text="ON").exists:
                self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                       text="ON").click.wait()
            self.d(text="Set date").click.wait()
            self.d(
                resourceId="android:id/date_picker_header_year").click.wait()

            # Set date as 2015-1-20
            year = "2014"
            if not self.d(resourceId="android:id/text1", text=year).exists:
                self.d(scrollable=True).scroll.to(
                    resourceId="android:id/text1", text=year)
            self.d(resourceId="android:id/text1", text=year).click.wait()
            time.sleep(2)
            date = "20"
            if self.d(className="android.view.View", text=date).exists:
                self.d(className="android.view.View", text=date).click.wait()
                time.sleep(2)
                count = 0
                flag = self.d(description="20 January 2015", text="20").exists
                while not flag:
                    self.d(className="android.widget.ImageButton",
                           resourceId="android:id/next").click.wait()
                    self.d(className="android.view.View",
                           text=date).click.wait()
                    time.sleep(1)
                    if self.d(text="Tue, Jan 20").exists:
                        break
                    time.sleep(1)
                    count += 1
                    if count > 13:
                        break
                self.d(text="OK").click.wait()
                time.sleep(2)
                result = self.d(text="January 20, 2015").exists
                time.sleep(2)
                self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                       text="OFF").click.wait()
                self.assertTrue(result)
        except Exception as e:
            self.clear.clearAll()
            print(e)
        finally:
            self.clear.clearAll()

    def testSetTime(self):
        """Check the time show"""
        try:
            # version = self.settingscommon.get_android_version_model()
            self.settingscommon.open_date_time()
            if self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                      text="ON").exists:
                self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                       text="ON").click.wait()
            self.d(text="Set time").click.wait()

            # Set time as 6:15 AM
            self.d(resourceId="android:id/am_label", text="AM").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                   index="5").click.wait()
            """
            if version == "8.1.0":
                # 15 correspond to index 4 in "8.1.0"
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                index="4").click.wait()
            else:
                # 15 correspond to index 3 in "8.0.0"
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                index="3").click.wait()
            """
            self.d(description="15").click.wait()
            self.d(resourceId="android:id/button1", text="OK").click.wait()
            time.sleep(2)
            self.assertTrue(self.d(resourceId="android:id/summary",
                                   text="6:15 AM").exists)

            # verify and set time as 10:40 PM
            self.d(resourceId="android:id/summary",
                   text="6:15 AM").click.wait()
            self.d(resourceId="android:id/pm_label", text="PM").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                   index="9").click.wait()
            """"
            if version == "8.1.0":
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                index="9").click.wait()
            else:
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                index="8").click.wait()
            """
            self.d(description="40").click.wait()
            time.sleep(2)
            self.d(resourceId="android:id/button1", text="OK").click.wait()
            result = self.d(resourceId="android:id/summary",
                            text="10:40 PM").exists
            time.sleep(2)
            self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                   text="OFF").click.wait()
            self.assertTrue(result)
        finally:
            self.clear.clearAll()

    def checkSystemTimeAfterReboot(self):
        try:
            # check the time after reboot system
            os.system(u'adb -s ' + self.serialno + ' reboot')
            time.sleep(30)
            runtimeget = os.popen(u'adb -s ' + self.serialno + ' shell cat /proc/uptime')
            list = runtimeget.read()
            list1 = list.split()
            platform = self.common.getProductName()
            time.sleep(2)
            if (float(list1[0]) < 60):
                if platform == 'mek_8q':
                    if self.d(
                             resourceId="com.android.systemui:id/clock_view",
                             text="1040").exists or self.d(
                             resourceId="com.android.systemui:id/clock_view",
                             text="1041").exists:
                        result = True
                    else:
                        result = False
                    time.sleep(2)
                    self.d.swipe(400, 400, 400, 20, steps=10)
                elif platform == 'mek_8q_car':
                    if self.d(
                            resourceId="com.android.systemui:id/clock",
                            text="10:40").exists or self.d(
                            resourceId="com.android.systemui:id/clock",
                            text="10:41").exists:
                        result = True
                    else:
                        result = False
                expected_result = True
                self.assertEqual(result, expected_result)
            else:
                print("something wrong with reboot system!")
        except Exception as e:
            print(e)
        finally:
            self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                   text="OFF").click.wait()

    def testSelectTimeZone(self):
        """Check the time zone"""
        self.settingscommon.open_date_time()
        # Select new time zone
        self.d(text="Select time zone").click.wait()
        self.d(scrollable=True).scroll.to(text="Shanghai")
        self.d(text="Shanghai").click.wait()
        result = self.d(text="GMT+08:00 China Standard Time").exists
        self.assertTrue(result)
        self.clear.clearAll()

    def testSetMultipleAlarms(self):
        """GEN_DAT_00025	User can set multiple alarms
           GEN_DAT_00020	User can add/change/delete alarm for specified
                            date/time/repeat
                            pattern and alerting methods (ringtone/vibrate)
        """
        DATA_LIST = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                     'Friday', 'Saturday']
        # version = self.settingscommon.get_android_version_model()
        self.settingscommon.display()
        self.d.press.back()
        self.d.press.back()
        time.sleep(2)
        try:
            if self.d(text="Clock").exists:
                self.d(text="Clock").click.wait()
                self.d(text="ALARM").click.wait()
                if not self.d(text="No Alarms").exists:
                    # if the alarms have been set ,clear all of them
                    while self.d(packageName="com.android.deskclock").exists:
                        if self.d(description="Expand alarm").exists:
                            self.d(description="Expand alarm").click.wait()
                        if self.d(text="Delete").exists:
                            self.d(text="Delete").click.wait()
                        if self.d(text="No Alarms").exists:
                            break
                self.d(description="Add alarm").click.wait()
                # Set time as 6:15 AM on Tuesday with Barium alerting
                self.d(resourceId="android:id/am_label",
                       text="AM").click.wait()
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                       index="5").click.wait()
                self.d(description="15").click.wait()
                self.d(resourceId="android:id/button1",
                       text="OK").click.wait()
                self.d(text="Repeat").click.wait()
                # uncheck the date
                for i in range(len(DATA_LIST)):
                    if self.d(description=DATA_LIST[i],
                              checked="false").exists:
                        continue
                    if i == 3:
                        continue
                    self.d(description=DATA_LIST[i]).click.wait()
                self.d(resourceId="com.android.deskclock:id/choose_ringtone")\
                    .click.wait()
                self.d(scrollable=True).scroll.to(text="Barium")
                self.d(text="Barium").click.wait()
                self.d.press("back")
                if self.d(text="6:15 AM") and self.d(text="Barium") and \
                   self.d(text="W", checked="true"):
                    result = True
                else:
                    result = False
                self.assertTrue(result)
                # add multiple alarms
                self.d(description="Add alarm").click.wait()
                self.d(resourceId="android:id/pm_label",
                       text="PM").click.wait()
                self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                       index="6").click.wait()
                self.d(description="15").click.wait()
                self.d(resourceId="android:id/button1",
                       text="OK").click.wait()
                self.assertTrue(self.d(text="7:15 PM").exists)
        finally:
            self.clear.clearAll()

    def testAlarmWakeupDevice(self):
        # version = self.settingscommon.get_android_version_model()
        self.settingscommon.open_date_time()
        if self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                  text="ON").exists:
            self.d(className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                   text="ON").click.wait()
        self.d(text="Set time").click.wait()

        # Set time as 6:15 AM
        self.d(resourceId="android:id/am_label", text="AM").click.wait()
        self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
               index="5").click.wait()
        self.d(description="15").click.wait()
        self.d(resourceId="android:id/button1", text="OK").click.wait()
        # set sleep time
        self.d.press.back()
        self.d.press.back()
        self.d(text="Display").click.wait()
        self.d(text="Sleep").click.wait()
        self.d(text="15 seconds").click.wait()
        self.d.press.back()
        self.d.press.back()

        # set alarm at 6:16 AM
        if self.d(text="Clock").exists:
            self.d(text="Clock").click.wait()
            self.d(text="ALARM").click.wait()
            if not self.d(text="No Alarms").exists:
                while self.d(packageName="com.android.deskclock").exists:
                    if self.d(description="Expand alarm").exists:
                        self.d(description="Expand alarm").click.wait()
                    if self.d(text="Delete").exists:
                        self.d(text="Delete").click.wait()
                    if self.d(text="No Alarms").exists:
                        break
            self.d(description="Add alarm").click.wait()
            self.d(resourceId="android:id/am_label", text="AM").click.wait()
            self.d(className=self.settingscommon.ANDROID_WIDGET_RADIAL,
                   index="5").click.wait()
            self.d(resourceId="android:id/toggle_mode").click.wait()
            # self.d(resourceId="android:id/input_minute").click.bottomright()
            #  click right bottom of the interface input box
            self.d(resourceId="android:id/input_minute").clear_text()
            time.sleep(2)
            self.d(resourceId="android:id/input_minute",
                   className="android.widget.EditText",
                   index="2").set_text("16")
            self.d(text="OK").click.wait()
            if self.d(text="Repeat", checked="false").exists:
                self.d(text="Repeat", checked="false").click.wait()
            time.sleep(60)
            self.d.wakeup()
            time.sleep(2)
            self.d.swipe(0, 0, 0, 400)
            time.sleep(2)
            self.d.swipe(0, 0, 0, 400)
            if self.d(resourceId="android:id/title", text="Alarm").exists:
                result = True
            else:
                result = False
            self.assertTrue(result)
            self.d(resourceId="android:id/title", text="Alarm").click.wait()
            time.sleep(2)
            if self.d(text="GOT IT").exists:
                self.d(text="GOT IT").click.wait()
            # drag the ui object to another ui object(center)
            self.d(resourceId="com.android.deskclock:id/alarm").drag.to(
                resourceId="com.android.deskclock:id/dismiss")
            time.sleep(2)
        self.clear.clearAll()

# ===========================================================================
#  test Screen Lock
# ===========================================================================

    def testPatternScreenLock(self):
        # version = self.settingscommon.get_android_version_model()
        platform = os.popen(u'adb -s ' + self.serialno + ' shell \
                    getprop ro.product.name').read().strip()
        self.yamldata = self.settingscommon.get_yaml_data()
        if platform == "mek_8q":
            # delete point coordinate
            self.dp_x1 = self.dp_x2 = int(
                self.yamldata['mek_8q']['testPatternLock']['dp'][0])
            self.dp_y1 = int(
                self.yamldata['mek_8q']['testPatternLock']['dp'][1])
            self.dp_y2 = self.dp_y3 = int(
                self.yamldata['mek_8q']['testPatternLock']['dp'][3])
            self.dp_x3 = self.dp_x4 = int(
                self.yamldata['mek_8q']['testPatternLock']['dp'][6])
            self.dp_y4 = int(
                self.yamldata['mek_8q']['testPatternLock']['dp'][7])
            # set point coordinate
            self.sp_x1 = self.sp_x2 = int(
                self.yamldata['mek_8q']['testPatternLock']['sp'][0])
            self.sp_y1 = int(
                self.yamldata['mek_8q']['testPatternLock']['sp'][1])
            self.sp_y2 = self.sp_y3 = int(
                self.yamldata['mek_8q']['testPatternLock']['sp'][3])
            self.sp_x3 = self.sp_x4 = int(
                self.yamldata['mek_8q']['testPatternLock']['sp'][6])
            self.sp_y4 = int(
                self.yamldata['mek_8q']['testPatternLock']['sp'][7])
            # open point coordinate
            self.op_x1 = self.op_x2 = int(
                self.yamldata['mek_8q']['testPatternLock']['op'][0])
            self.op_y1 = int(
                self.yamldata['mek_8q']['testPatternLock']['op'][1])
            self.op_y2 = self.op_y3 = int(
                self.yamldata['mek_8q']['testPatternLock']['op'][3])
            self.op_x3 = self.op_x4 = int(
                self.yamldata['mek_8q']['testPatternLock']['op'][6])
            self.op_y4 = int(
                self.yamldata['mek_8q']['testPatternLock']['op'][7])
        elif platform == "mek_6q":
            # delete point coordinate
            self.dp_x1 = self.dp_x2 = int(
                self.yamldata['mek_6q']['testPatternLock']['dp'][0])
            self.dp_y1 = int(
                self.yamldata['mek_6q']['testPatternLock']['dp'][1])
            self.dp_y2 = self.dp_y3 = int(
                self.yamldata['mek_6q']['testPatternLock']['dp'][3])
            self.dp_x3 = self.dp_x4 = int(
                self.yamldata['mek_6q']['testPatternLock']['dp'][6])
            self.dp_y4 = int(
                self.yamldata['mek_6q']['testPatternLock']['dp'][7])
            # set point coordinate
            self.sp_x1 = self.sp_x2 = int(
                self.yamldata['mek_6q']['testPatternLock']['sp'][0])
            self.sp_y1 = int(
                self.yamldata['mek_6q']['testPatternLock']['sp'][1])
            self.sp_y2 = self.sp_y3 = int(
                self.yamldata['mek_6q']['testPatternLock']['sp'][3])
            self.sp_x3 = self.sp_x4 = int(
                self.yamldata['mek_6q']['testPatternLock']['sp'][6])
            self.sp_y4 = int(
                self.yamldata['mek_6q']['testPatternLock']['sp'][7])
            # open point coordinate
            self.op_x1 = self.op_x2 = int(
                self.yamldata['mek_6q']['testPatternLock']['op'][0])
            self.op_y1 = int(
                self.yamldata['mek_6q']['testPatternLock']['op'][1])
            self.op_y2 = self.op_y3 = int(
                self.yamldata['mek_6q']['testPatternLock']['op'][3])
            self.op_x3 = self.op_x4 = int(
                self.yamldata['mek_6q']['testPatternLock']['op'][6])
            self.op_y4 = int(
                self.yamldata['mek_6q']['testPatternLock']['op'][7])
        elif platform == "evk_6sl":
            # delete point coordinate
            self.dp_x1 = self.dp_x2 = int(
                self.yamldata['evk_6sl']['testPatternLock']['dp'][0])
            self.dp_y1 = int(
                self.yamldata['evk_6sl']['testPatternLock']['dp'][1])
            self.dp_y2 = self.dp_y3 = int(
                self.yamldata['evk_6sl']['testPatternLock']['dp'][3])
            self.dp_x3 = self.dp_x4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['dp'][6])
            self.dp_y4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['dp'][7])
            # set point coordinate
            self.sp_x1 = self.sp_x2 = int(
                self.yamldata['evk_6sl']['testPatternLock']['sp'][0])
            self.sp_y1 = int(
                self.yamldata['evk_6sl']['testPatternLock']['sp'][1])
            self.sp_y2 = self.sp_y3 = int(
                self.yamldata['evk_6sl']['testPatternLock']['sp'][3])
            self.sp_x3 = self.sp_x4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['sp'][6])
            self.sp_y4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['sp'][7])
            # open point coordinate
            self.op_x1 = self.op_x2 = int(
                self.yamldata['evk_6sl']['testPatternLock']['op'][0])
            self.op_y1 = int(
                self.yamldata['evk_6sl']['testPatternLock']['op'][1])
            self.op_y2 = self.op_y3 = int(
                self.yamldata['evk_6sl']['testPatternLock']['op'][3])
            self.op_x3 = self.op_x4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['op'][6])
            self.op_y4 = int(
                self.yamldata['evk_6sl']['testPatternLock']['op'][7])
        else:
            raise Exception("No corresponding version and model exist,Please "
                            "check if current version and model has been "
                            "added in the list.")
        try:
            self.d.press.back()
            self.d.press.back()
            # delete screen password if it exists.
            dp = [(self.dp_x1, self.dp_y1),
                  (self.dp_x2, self.dp_y2),
                  (self.dp_x3, self.dp_y3),
                  (self.dp_x4, self.dp_y4)]
            self.d(text="Security & location").click.wait()
            self.d(text="Screen lock").click.wait()
            if self.d(resourceId="com.android.settings:id/headerText",
                      text="Confirm your pattern").exists:
                time.sleep(2)
                self.d.swipePoints(dp)
                time.sleep(3)
                self.d(text="None").click.wait()
                self.d(resourceId="android:id/button1",
                       text="YES, REMOVE").click.wait()
                time.sleep(2)
                self.d(text="Screen lock").click.wait()
            # set pattern lock
            self.d(text="Pattern").click.wait()
            if self.d(text="Draw an unlock pattern").exists:
                spoints = [(self.sp_x1, self.sp_y1),
                           (self.sp_x2, self.sp_y2),
                           (self.sp_x3, self.sp_y3),
                           (self.sp_x4, self.sp_y4)]
                # spoints = [(420, 325), (420, 420), (600, 420), (600, 510)]
                self.d.swipePoints(spoints)
                time.sleep(2)
                if self.d(text="Pattern recorded").exists and self.d(
                        text="NEXT", enabled="true").exists:
                    self.d(text="NEXT").click.wait()
                    if self.d(
                            text="Draw pattern again to confirm"
                    ).exists and self.d(text="CONFIRM",
                                        enabled="false").exists:
                        self.d.swipePoints(spoints)
                        time.sleep(2)
                        self.d(text="CONFIRM").click.wait()
                        time.sleep(2)
                        if self.d(
                                resourceId="com.android.settings:id/show_all",
                                checked="false").exists:
                            self.d(
                                resourceId="com.android.settings:id/show_all",
                                checked="false").click.wait()
                        self.d(text="DONE").click.wait()
            # test pattern lock
            time.sleep(2)
            self.d.screen.off()
            time.sleep(4)
            self.d.screen.on()
            self.d.swipe(400, 400, 400, 20, steps=10)
            time.sleep(2)
            self.d.swipe(400, 400, 400, 20, steps=10)
            if self.d(
                resourceId="com.android.systemui:id/lockPatternView"
            ).exists:
                time.sleep(2)
                op = [(self.op_x1, self.op_y1),
                      (self.op_x2, self.op_y2),
                      (self.op_x3, self.op_y3),
                      (self.op_x4, self.op_y4)]
                # tp = [(385, 245), (385, 375), (640, 375), (640, 502)]
                self.d.swipePoints(op)
                time.sleep(2)
                self.assertTrue(self.d(text="Screen lock").exists)
                time.sleep(2)
            # delete pattern lock
            self.d(text="Screen lock").click.wait()
            if self.d(resourceId="com.android.settings:id/headerText",
                      text="Confirm your pattern").exists:
                time.sleep(2)
                # dp = [(395, 330), (395, 445), (627, 445), (627, 560)]
                self.d.swipePoints(dp)
                time.sleep(3)
                self.d(text="None").click.wait()
                self.d(resourceId="android:id/button1",
                       text="YES, REMOVE").click.wait()
                time.sleep(2)
            else:
                self.assertTrue(False)
            self.clear.clearAll()
            self.assertTrue(True)
        except Exception as e:
            self.clear.clearAll()
            print(e)
            raise

    def testScreenPasswordLock(self):
        version = self.settingscommon.get_android_version_model()
        platform = os.popen(u'adb -s ' + self.serialno + ' shell \
                    getprop ro.product.name').read().strip()
        self.yamldata = self.settingscommon.get_yaml_data()
        if platform == "mek_8q":
            self.p_x = int(self.yamldata['mek_8q']['testPasswordLock'][0])
            self.p_y = int(self.yamldata['mek_8q']['testPasswordLock'][1])
        elif platform == "mek_6q":
            self.p_x = int(self.yamldata['mek_6q']['testPasswordLock'][0])
            self.p_y = int(self.yamldata['mek_6q']['testPasswordLock'][1])
        elif platform == "evk_6sl":
            self.p_x = int(self.yamldata['evk_6sl']['testPasswordLock'][0])
            self.p_y = int(self.yamldata['evk_6sl']['testPasswordLock'][1])
        else:
            assert False
        try:
            self.d.press.back()
            self.d.press.back()
            # delete screen password if it exists.
            self.d(text="Security & location").click.wait()
            self.d(text="Screen lock").click.wait()
            if self.d(
                resourceId="com.android.settings:id/suw_layout_title"
            ).exists:
                self.d(resourceId="com.android.settings:id/password_entry"
                       ).set_text("1234")
                # self.d.click(950, 520) # 6Q
                self.d.click(self.p_x, self.p_y)
                time.sleep(3)
                self.d(text="None").click.wait()
                self.d(resourceId="android:id/button1").click.wait()
                self.d(text="Screen lock").click.wait()

            # set new password
            self.d(text="Password").click.wait()
            self.d(resourceId="com.android.settings:id/password_entry"
                   ).set_text("1234")
            self.d(text="NEXT").click.wait()
            self.d(resourceId="com.android.settings:id/password_entry"
                   ).set_text("1234")
            self.d(text="OK").click.wait()
            time.sleep(2)
            if self.d(resourceId="com.android.settings:id/show_all",
                      checked="false").exists:
                self.d(resourceId="com.android.settings:id/show_all",
                       checked="false").click.wait()
            self.d(text="DONE").click.wait()
        except Exception as e:
            self.assertTrue(True)
        self.d.screen.off()
        time.sleep(4)
        self.d.screen.on()
        self.d.swipe(400, 400, 400, 20, steps=10)
        time.sleep(2)
        if self.d(resourceId="com.android.systemui:id/passwordEntry"
                  ).exists:
            self.d(resourceId="com.android.systemui:id/passwordEntry"
                   ).set_text("1234")
            time.sleep(2)
            # self.d.click(950, 520)
            # self.d.click(1780, 730)  # 8q
            self.d.click(self.p_x, self.p_y)
            time.sleep(2)
            self.assertTrue(True)

        # delete screen password and recover initial status.
        self.d(text="Security & location").click.wait()
        self.d(text="Screen lock").click.wait()
        if self.d(resourceId="com.android.settings:id/suw_layout_title"
                  ).exists:
            self.d(resourceId="com.android.settings:id/password_entry"
                   ).set_text("1234")
            # self.d.click(950, 520)
            # self.d.click(1780, 730)  # 8q
            self.d.click(self.p_x, self.p_y)
            time.sleep(3)
            self.d(text="None").click.wait()
            self.d(resourceId="android:id/button1").click.wait()
        self.clear.clearAll()

    def testPinLock(self):
        # version = self.settingscommon.get_android_version_model()
        platform = os.popen(u'adb -s ' + self.serialno + ' shell \
            getprop ro.product.name').read().strip()
        self.yamldata = self.settingscommon.get_yaml_data()
        if platform == "mek_8q":
            self.p_x = int(self.yamldata['mek_8q']['testPinLock'][0])
            self.p_y = int(self.yamldata['mek_8q']['testPinLock'][1])
        elif platform == "mek_6q":
            self.p_x = int(self.yamldata['mek_6q']['testPinLock'][0])
            self.p_y = int(self.yamldata['mek_6q']['testPinLock'][1])
        elif platform == "evk_6sl":
            self.p_x = int(self.yamldata['evk_6sl']['testPinLock'][0])
            self.p_y = int(self.yamldata['evk_6sl']['testPinLock'][1])
        else:
            raise Exception('No corresponding version exist,Please check '
                            'if current version has been added')
        try:
            try:
                self.d.press.back()
                self.d.press.back()
                self.d(text="Security & location").click.wait()
                self.d(text="Screen lock").click.wait()
                self.d(textContains="PIN").click.wait()
                self.d(resourceId="com.android.settings:id/password_entry"
                       ).set_text("1234")
                time.sleep(3)
                self.d(text="NEXT").click.wait()
                time.sleep(2)
                self.d(resourceId="com.android.settings:id/password_entry"
                       ).set_text("1234")
                self.d(text="OK").click.wait()
                time.sleep(2)
                if self.d(resourceId="com.android.settings:id/show_all",
                          checked="false").exists:
                    self.d(resourceId="com.android.settings:id/show_all",
                           checked="false").click.wait()
                self.d(text="DONE").click.wait()
                time.sleep(3)
            except Exception as e:
                self.assertTrue(False)

            self.d.screen.off()
            time.sleep(4)
            self.d.screen.on()
            self.d.swipe(400, 400, 400, 20, steps=10)
            time.sleep(2)
            for i in range(1, 5):
                self.d(text=str(i)).click.wait()
            time.sleep(2)
            self.d(resourceId="com.android.systemui:id/key_enter"
                   ).click.wait()
            time.sleep(2)

            # delete screen password and recover initial status.
            self.d(text="Security & location").click.wait()
            self.d(text="Screen lock").click.wait()
            if self.d(resourceId="com.android.settings:id/suw_layout_title"
                      ).exists:
                self.d(resourceId="com.android.settings:id/password_entry"
                       ).set_text("1234")
                self.d.click(self.p_x, self.p_y)
                time.sleep(3)
                self.d(text="None").click.wait()
                self.d(resourceId="android:id/button1").click.wait()
        except Exception as e:
            print(e)
            self.assertFalse(True)
        self.clear.clearAll()

    def testTimeOut(self):
        """
            PM_SW_00010	User can set screen timeout time in setting UI
            PM_SW_00011	After screen timeout, system should suspend
            PM_SW_00026	System should be able to wakeup by HOME key
        """
        self.settingscommon.display()
        self.d(text="Sleep").click.wait()
        self.d(text="15 seconds").click.wait()
        self.d.press.back()
        self.settingscommon.open_screen_saver()
        self.d(text="Current screen saver").click.wait()
        self.d(text="Clock").click.wait()
        self.d(text="When to start").click.wait()
        self.d(text="While charging").click.wait()
        self.d.press.back()
        self.d.press.back()
        self.d(scrollable=True).scroll.to(text="System")
        self.d(text="System").click.wait()
        if not self.d(text="Developer options").exists:
            self.d(text="About tablet").click.wait()
            time.sleep(2)
            for i in range(7):
                self.d(text="Build number").click()
            self.d.press.back()
        self.d(text="Developer options").click.wait()
        if self.d(index="3").child(
                className=self.settingscommon.ANDROID_WIDGET_SWITCH,
                checked="true").exists:
            self.d(text="Stay awake").click.wait()
        time.sleep(20)
        try:
            if self.d(text="GOT IT").wait.exists():
                self.d(text="GOT IT").click.wait()
            if self.d(resourceId=self.settingscommon.DESKCLOCK_CLOCK).exists:
                result = True
            else:
                result = False
            self.assertTrue(result)
        finally:
            set_up(self.platform, self.d, self.serialno)
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            self.d(text="Developer options").click.wait()
            self.d(text="Stay awake").click.wait()
            self.clear.clearAll()

# ===========================================================================
# Steps:
# 1. Open soundrecorder.apk,click twice 'allow'
# 2. click reset app,then check if sound recorder has been reset
# 3. check if exist two 'allow '
# ===========================================================================

    def testInstallAPK(self):
        try:
            files = os.listdir(r'.')
            print(files)
            for file in files:
                # print(file[len(file)-3:len(file)])
                if file[len(file) - 3:len(file)] == "apk":
                    print(file)
                    string = 'adb install ' + file
                    print(string)
                    os.system(string)
            time.sleep(6)
        except Exception as e:
            print(e)
            raise

    def testInstall(self):
        try:
            os.system(u'adb -s ' +
                      self.serialno + ' shell am start com.android.soundrecorder/.SoundRecorder')
            time.sleep(2)
            if self.d(text="ALLOW").exists:
                self.d(text="ALLOW").click.wait()
                time.sleep(3)
                if self.d(text="ALLOW").exists:
                    self.d(text="ALLOW").click.wait()
                    time.sleep(3)
                    result = True
                    print("Open sound recorder successfully")
                else:
                    print("Not find 2nd ALLOW window")
                    result = True
            elif self.d(text="Record your message").exists:
                result = True
                print("sound recorder had been open")
            else:
                result = False
                print("Not find ALLOW window")
            expected_result = True
            self.assertEqual(result, expected_result)
        except Exception as e:
            print(e)
            raise

    def testResetapp(self):
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            time.sleep(2)
            self.d(text="Reset options").click.wait()
            time.sleep(2)
            self.d(text="Reset app preferences").click.wait()
            time.sleep(2)
            self.d(text="RESET APPS", resourceId="android:id/button1"
                   ).click.wait()
            time.sleep(2)
            os.system(u'adb -s ' + self.serialno + ' shell am start com.android.soundrecorder/.SoundRecorder')
            time.sleep(2)
            if self.d(text="ALLOW").exists:
                self.d(text="ALLOW").click.wait()
                time.sleep(2)
                if self.d(text="ALLOW").exists:
                    self.d(text="ALLOW").click.wait()
                    time.sleep(2)
                    result = True
                    print("Reset app successfully")
                else:
                    result = False
                    print("Reset app unsuccessfully")
            else:
                result = False
                print("Reset app unsuccessfully")
            expected_result = True
            self.assertEqual(result, expected_result)
        except Exception as e:
            print(e)
            raise

# ===========================================================================
# test Reset Tablet
# ===========================================================================

    def testResetTablet(self):
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            time.sleep(2)
            self.d(text="Reset options").click.wait()
            time.sleep(2)
            self.d(className="android.widget.LinearLayout", index="2",
                   clickable="true").click.wait()
            time.sleep(2)
            self.d(text="RESET TABLET").click.wait()
            time.sleep(2)
            self.d(text="ERASE EVERYTHING").click.wait()
            time.sleep(100)
        except Exception as e:
            print(e)
            raise

    def testAPPNumber(self):
        try:
            self.d(resourceId="com.android.settings:id/dashboard_tile",
                   className="android.widget.LinearLayout",
                   index="2").click.wait()
            if self.d(text="See all 23 apps").exists or self.d(
                    text="19 apps installed").exists:
                result = True
                print("Reset TABLET pass")
            else:
                result = False
                print("Reset TABLET fail")
            expected_result = True
            self.assertEqual(result, expected_result)
        except Exception as e:
            print(e)
            raise

# ===========================================================================
#  Move the picture to the Android device
# ===========================================================================

    def copyFiles2(self):
        srcPath = os.path.join(os.path.abspath(os.path.dirname(__file__)) +
                               '/../resource/image/')
        dstPath = '/sdcard/Pictures'
        if not os.path.exists(srcPath):
            raise Exception("src path not exist!")
        # if not os.path.exists(dstPath):
        #     os.makedirs(dstPath)
        for root, dirs, files in os.walk(srcPath):
            for file in files:
                copy(os.path.join(root, file), dstPath)
                # path = os.path.join(srcPath, file)
                os.system(u'adb -s ' + self.serialno + srcPath +
                          'LargePic000.bmp/sdcard/Pictures')
                print(file + "copy succeeded")
