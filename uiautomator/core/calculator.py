#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.
#
# Author:Faye Dong
# Created Date:3/13/2018
# Updated Date:6/14/2018
#
# Description:Android Calculator app UI test script
# -----------------------------------------------------------------------------------------------
# Note:
# 1.Please install python3, pyusb, uiautomator before testing;
# 2.Please check if the DUT support monkey tool;
# 3.Please refer to below link for details:
# 4.https://developer.android.com/studio/test/monkey.html
# -----------------------------------------------------------------------------------------------
from clear import Clear
from common import Common
from uiautomator import Device
import os
import time
import unittest


class Calculator(unittest.TestCase):
    def __init__(self, name, serial_no):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serial_no)
        self.serial_no = serial_no

        self.serial_no = serial_no
        self.clear = Clear(self.d, self.serial_no)
        self.clear.d = self.d
        self.clear.serial_no = self.serial_no

        self.common = Common(self.d, self.serial_no)
        self.common.d = self.d
        self.common.serial_no = self.serial_no

    def setUp(self):
        try:
            self.d.wakeup()
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise

    # ===================================================================================================
    # Case ID: Android_Calculator_02
    # Description: Test Calculator app rotation
    # Notes: Please ensure the DUT support rotation
    # ===================================================================================================
    def testRotation(self):
        try:
            #os.system(u'adb -s ' + self.serial_no
            #         + ' shell am start com.android.calculator2/.Calculator')
            self.common.open_calculator()
            time.sleep(2)

            content = self.common.get_rotation_status()
            if int(content) == 1:
                self.common.set_rotation_status(0)
                time.sleep(5)

            self.common.set_rotation_value(3)
            time.sleep(2)
            value = self.common.get_rotation_value()
            self.assertEqual(int(value), 3)

            self.common.set_rotation_value(2)
            time.sleep(2)
            value = self.common.get_rotation_value()
            self.assertEqual(int(value), 2)

            self.common.set_rotation_value(1)
            time.sleep(2)
            value = self.common.get_rotation_value()
            self.assertEqual(int(value), 1)

            self.common.set_rotation_value(0)
            time.sleep(2)
            value = self.common.get_rotation_value()
            self.assertEqual(int(value), 0)

            content = self.common.get_rotation_status()
            if int(content) == 0:
                self.common.set_rotation_status(1)
                time.sleep(2)
        except Exception as e:
            print(e)
            raise

    # ===================================================================================================
    # Case ID: Android_Calculator_01
    # Description: Random click on Calculator app by using monkey tool
    # ===================================================================================================
    def testMonkey(self):
        try:
            os.system(u'adb -s ' + self.serial_no
                      + ' shell monkey -p com.android.calculator2 '
                        '--throttle 500 --pct-touch 100 '
                        '--ignore-native-crashes -v -v 1000 >monkey.txt')
            time.sleep(2)

            r = os.system('grep -E "ANR|CRASH|GC|Exception" monkey.txt')
            r = r >> 8
            print(r)
            self.assertEqual(r, 1)
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.wakeup()
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise
