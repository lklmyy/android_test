#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# ----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Ryan Yan
# Date: 5/21/2018
#
# Description: Android Fastboot test script
# ----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure run it on a clean board environment;
# 2. Please ensure the PC you used has installed python3 and uiautomator, unittest;
# 3. Should copy needed images to /images folder before test
# 4. System under test should support avb function.
# ----------------------------------------------------------------------------------------------

import os
import time
import unittest

from uiautomator import Device


class Common:
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno

    def settings(self):
        platform = os.popen(u'adb -s ' + self.serialno + '  shell getprop ro.product.name').read().strip()
        try:
            self.d.wakeup()
            self.d.swipe(400, 400, 400, 20, steps=10)
            self.d.press.home()
            if platform == 'mek_8q_car':
                print('mek_8q_car')
                os.system(u'adb -s ' + self.serialno + ' shell am start -n com.android.settings/com.android.settings.Settings')
            elif platform == 'mek_8q':
                self.d.swipe(400, 400, 400, 20, steps=10)
                self.d(text="Settings", packageName="com.android.launcher3").click()
        except Exception as e:
            print(e)
            raise

    def developer(self):
        self.settings()
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            if self.d(text="Developer options").exists:
                print("you are already a developer")
            else:
                self.d(text="About tablet").click.wait()
                for i in range(7):
                    self.d(text="Build number").click.wait()
                print("you are a developer now")
        except Exception as e:
            print(e)
            raise

    def oemunlock(self, value):
        self.developer()
        self.settings()
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            self.d(text="Developer options").click.wait()
            self.d(scrollable=True).scroll.to(text="OEM unlocking")
            switch = self.d(text="OEM unlocking").right(resourceId="android:id/switch_widget")
            if switch and switch.info[u"checked"] != value:
                switch.click.wait()
                if self.d(text="ENABLE").exists:
                    self.d(text="ENABLE").click.wait()
                    print("OEM unlocking enable")
            print("OEM unlocking is setting to", switch.info[u"checked"])
        except Exception as e:
            print(e)
            raise


class Fastboot(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.settings()
        print("setup done")

    def oemlock(self):
        '''oem lock'''
        self.common.oemunlock(value=False)
        r = os.system('./flash_8qm.sh')
        if r == 0:
            result = False
        else:
            result = True
            try:
                os.system(u'fastboot -s ' + self.serialno + ' reboot')
                time.sleep(40)
            except Exception as e:
                print(e)
        self.assertTrue(result)

    def upgrade(self):
        '''upgrade image via fastboot'''
        self.common.oemunlock(value=True)
        curVer = os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.build.date').read()
        print(curVer)
        curVerUTC = int(os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.build.date.utc').read())
        print(curVerUTC)
        r = os.system('./flash_8qm.sh')
        if r == 0:
            os.system(u'adb -s ' + self.serialno + ' wait-for-device')
            time.sleep(40)
            newVer = os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.build.date').read()
            print(newVer)
            newVerUTC = int(os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.build.date.utc').read())
            print(newVerUTC)
            if curVerUTC < newVerUTC:
                result = True
            else:
                result = False
        else:
            print('flash script executed failed')
            result = False
            try:
                os.system(u'fastboot -s ' + self.serialno + ' reboot')
                time.sleep(40)
            except Exception as e:
                print(e)
        self.assertTrue(result)

    def tearDown(self):
        self.d.press.home()
        time.sleep(5)
        print("tearDown")
