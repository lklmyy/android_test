#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Fiona Ye
# Created Date: 3/23/2018
# Updated Date: 6/21/2018
#
# Description: Android Camera stress cases
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure run it on a clean board environment;
# 2. Please ensure the PC you used has installed python3 and uiautomator;
# 3. Please ensure all cameras connected to the test board;
# 4. Please check the screenshots after run pass;
# -----------------------------------------------------------------------------------------------

from clear import Clear
from common import Common
from uiautomator import Device
import time
import unittest


class CameraStressCommon:
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def change_function_stress(self, ori):
        try:
            for i in range(20):
                # change between back camera and back camcorder 20 times
                num = str(i)
                self.common.change_function("Camera")
                self.d.screenshot(num + "_" + ori + "_CVchange_Camera_Preview.png")
                self.common.change_function("Video")
                self.d.screenshot(num + "_" + ori + "_CVchange_Video_Preview.png")
        except Exception as e:
            print(e)
            raise


class Prepare(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.clear = Clear(self.d, self.serialno)
        self.clear.d = self.d
        self.clear.serialno = self.serialno

    def preparation(self):
        self.clear.clearAll()
        self.clear.clearGallery()


class BackcamcorderStress(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cscommon = CameraStressCommon(self.d, self.serialno)
        self.cscommon.d = self.d
        self.cscommon.serialno = self.serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.open_camera()
        print("setup done")

    def testChangetoBackCamcorder(self):
        self.common.change_function("Video")
        self.common.change_to_back()

# ***************************************************************************************************
# Description: Do 20 times switch among "back camcoder" and "back camera"(natural orientation)
# ***************************************************************************************************
    def testBackChangeCVStressN(self):
        self.common.change_to_back()
        self.cscommon.change_function_stress("Back")

# ***************************************************************************************************
# Description: Switch among different video quality 20 times: 1080, 720, 480
# ***************************************************************************************************
    def testBackCamcorderQualityStress(self):
        self.testChangetoBackCamcorder()
        try:
            for i in range(20):        # change quality for 20 times
                num = str(i)
                self.common.switch_quality("Back camera video", "HD 1080p")
                self.common.switch_quality("Back camera video", "HD 720p")
                self.common.switch_quality("Back camera video", "SD 480p")
                time.sleep(0.5)
                self.d.screenshot(num + "_BackCamcorder_switch_quality_preview.png")
                # png as test result
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In back camcorder recording 100 times in max resolution at i.mx6
# ***************************************************************************************************
    def testBackCamcorderRecordingStress1080(self):
        self.testChangetoBackCamcorder()
        try:
            self.common.switch_quality("Back camera video", "HD 1080p")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(1, 5, num + "_BackCamcorder_1080_recording")
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In back camcorder recording 100 times in max resolution at 8QM
# ***************************************************************************************************
    def testBackCamcorderRecordingStress720(self):
        self.testChangetoBackCamcorder()
        try:
            self.common.switch_quality("Back camera video", "HD 720p")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(1, 5, num + "_BackCamcorder_720_recording")
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)


class BackcameraStress(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.open_camera()
        print("setup done")

    def testChangetoBackCamera(self):
        self.common.change_function("Camera")
        self.common.change_to_back()

# ***************************************************************************************************
# Description: Stay in back Camera and switch among different picture quality 20 times.
# ***************************************************************************************************
    def testBackCameraQualityStress(self):
        self.testChangetoBackCamera()
        try:
            for i in range(20):        # change quality for 20 times
                num = str(i)
                self.common.switch_quality("Back camera photo",
                                           "(4:3) 5.0 megapixels")
                self.common.switch_quality("Back camera photo",
                                           "(4:3) 0.8 megapixels")
                self.common.switch_quality("Back camera photo",
                                           "(4:3) 0.3 megapixels")
                self.common.switch_quality("Back camera photo",
                                           "(16:9) 2.1 megapixels")
                self.common.switch_quality("Back camera photo",
                                           "(16:9) 0.9 megapixels")
                time.sleep(1)
                self.d.screenshot(num+"_BackCamera_switch_quality_preview.png")
                # png as test result
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In back camera taking picture 100 times in max resolution at i.mx6
# ***************************************************************************************************
    def testBackCameraCaptureStress5M(self):
        self.testChangetoBackCamera()
        try:
            self.common.switch_quality("Back camera photo",
                                       "(4:3) 5.0 megapixels")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(2, 0, num + "_BackCamera_5m_caputure")
                time.sleep(1)
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In back camera taking picture 100 times in max resolution at 8QM
# ***************************************************************************************************
    def testBackCameraCaptureStress1M(self):
        self.testChangetoBackCamera()
        try:
            self.common.switch_quality("Back camera photo",
                                       "(8:5) 1.0 megapixels")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(2, 0, num+"_BackCamera_1m_caputure")
                time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)


class Longtimestress(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

# ***************************************************************************************************
# Description: Record a video overnight then check this video file next morning
# ***************************************************************************************************
    def OvernightStress(self):
        self.common.open_camera()
        self.common.change_function("Video")
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle").click()
            # open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:
                # check if there is change option, change option exists that means there are 2 cameras
                if self.d(description="Front camera").exists:
                    self.d(description="Front camera").click.wait()
                    # set to aim direction option
                    print("SET TO BACK CAMCORDER")
                self.common.switch_quality("Back camera video", "HD 1080p")
            self.common.record_shoot(2, 0, "Overnightstress")
            time.sleep(3)
        except Exception as e:
            print(e)
        if self.d(resourceId="com.android.camera2:id/recording_time").exists:
            assert True
            print("START RECORDING SUCCEED")
        else:
            assert False


class FrontcamcorderStress(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cscommon = CameraStressCommon(self.d, self.serialno)
        self.cscommon.d = self.d
        self.cscommon.serialno = self.serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.open_camera()
        print("setup done")

    def testChangetoFrontCamcorder(self):
        self.common.change_function("Video")
        self.common.change_to_front()

# ***************************************************************************************************
# Description: Do 20 times switch among "front camcoder" and "front camera"(natural orientation)
# ***************************************************************************************************
    def testFrontChangeCVStressN(self):
        self.testChangetoFrontCamcorder()
        self.cscommon.change_function_stress("Front")

# ***************************************************************************************************
# Description: Switch among different video quality in front camcorder 20 times: 720, 480
# ***************************************************************************************************
    def testFrontCamcorderQualityStress(self):
        self.testChangetoFrontCamcorder()
        try:
            for i in range(20):
                # change quality for 20 times
                num = str(i)
                self.common.switch_quality("Front camera video", "HD 720p")
                self.common.switch_quality("Front camera video", "SD 480p")
                time.sleep(0.5)
                self.d.screenshot(num + "_FrontCamcorder_switch_quality_preview.png")
                # png as test result
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In front camcorder recording 100 times in max resolution
# ***************************************************************************************************
    def testFrontCamcorderRecordingStress(self):
        self.testChangetoFrontCamcorder()
        try:
            self.common.switch_quality("Front camera video", "HD 720p")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(1, 5, num + "_FrontCamcorder_720_recording")
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)


class FrontcameraStress(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.open_camera()
        print("setup done")

    def testChangetoFrontCamera(self):
        self.common.change_function("Camera")
        self.common.change_to_front()

# ***************************************************************************************************
# Description: Open front Camera and switch among different picture quality 20 times.
# ***************************************************************************************************
    def testFrontCameraQualityStress(self):
        self.testChangetoFrontCamera()
        try:
            for i in range(20):        # change quality for 20 times
                num = str(i)
                self.common.switch_quality("Front camera photo",
                                           "(4:3) 5.0 megapixels")
                self.common.switch_quality("Front camera photo",
                                           "(4:3) 0.8 megapixels")
                self.common.switch_quality("Front camera photo",
                                           "(4:3) 0.3 megapixels")
                self.common.switch_quality("Front camera photo",
                                           "(16:9) 2.1 megapixels")
                self.common.switch_quality("Front camera photo",
                                           "(16:9) 0.9 megapixels")
                time.sleep(0.5)
                self.d.screenshot(num + "_FrontCamera_switch_quality_preview.png")
                # png as test result
        except Exception as e:
            print(e)
            raise

# ***************************************************************************************************
# Description: In front camera taking picture 100 times in max resolution
# ***************************************************************************************************
    def testFrontCameraCaptureStress(self):
        self.testChangetoFrontCamera()
        try:
            self.common.switch_quality("Front camera photo",
                                       "(4:3) 5.0 megapixels")
            for i in range(100):
                num = str(i)
                self.common.record_shoot(2, 0, num+"_FrontCamera_5m_caputure")
                time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)
