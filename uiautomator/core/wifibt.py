#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# ----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Ryan Yan
# Date: 3/01/2018
#
# Description: Android WiFi、BT UI test script
# ----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure run it on a clean board environment;
# 2. Please ensure the PC you used has installed python3 and uiautomator, unittest;
# 3. Please replace SSID and Password in wifiConnect and wifiConnect2
# 4. Plase replace the WIFI AP's Gateway address to assert test result
# ----------------------------------------------------------------------------------------------

import os
import time
import unittest

from uiautomator import Device


class Common:
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno

    def settings(self):
        platform = os.popen(u'adb -s ' + self.serialno + '  shell getprop ro.product.name').read().strip()
        try:
            self.d.wakeup()
            self.d.swipe(400, 400, 400, 20, steps=10)
            self.d.press.home()
            if platform == 'mek_8q_car':
                print('mek_8q_car')
                os.system(u'adb -s ' + self.serialno + ' shell am start -n com.android.settings/com.android.settings.Settings')
            elif platform == 'mek_8q':
                self.d.swipe(400, 400, 400, 20, steps=10)
                self.d(text="Settings", packageName="com.android.launcher3").click()
        except Exception as e:
            print(e)
            raise

    def resetnetwork(self):
        self.settings()
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            self.d(text="Reset options").click.wait()
            self.d(text="Reset Wi-Fi, mobile & Bluetooth").click.wait()
            self.d(text="RESET SETTINGS").click.wait()
            self.d(text="RESET SETTINGS").click.wait()
            print("All network settings have been reset")
        except Exception as e:
            print(e)
            raise

    def wifisetting(self):
        self.settings()
        try:
            self.d(scrollable=True).scroll.to(text="Network & Internet")
            self.d(text="Network & Internet").click.wait()
            self.d(text="Wi‑Fi").click.wait()
        except Exception as e:
            print(e)
            raise

    def btsetting(self):
        self.settings()
        try:
            self.d(scrollable=True).scroll.to(text="Connected devices")
            self.d(text="Connected devices").click.wait()
            self.d(text="Bluetooth").click.wait()
        except Exception as e:
            print(e)
            raise

    def switchstress(self):
        try:
            for i in range(1, 21):
                self.d(className="android.widget.Switch").click.wait()
                time.sleep(2)
                if self.d(text="OFF").exists:
                    print(i, "Wi-Fi status is OFF")
                elif self.d(text="ON").exists:
                    print(i, "Wi-Fi status is ON")
            if self.d(text="OFF").exists:
                self.d(text="OFF").click.wait()
                print("Turn on WiFi/BT finally")
        except Exception as e:
            print(e)
            raise

    def connect(self, ssid, password):
        try:
            if self.d(text="OFF").exists:
                self.d(text="OFF").click.wait()
                print("Turn on WiFi")
            if not self.d(text=ssid).wait.exists():
                self.d(scrollable=True).scroll.to(text=ssid)
            self.d(text=ssid).click()
            time.sleep(3)
            self.d(resourceId="com.android.settings:id/password", className="android.widget.EditText").set_text(password)
            self.d(text="CONNECT").wait.exists(timeout=1000)
            self.d(text="CONNECT").click.wait(timeout=3000)
            time.sleep(30)
            self.d(text=ssid).click.wait()
            time.sleep(20)
        except Exception as e:
            print(e)
            raise

    def forget(self):
        try:
            if self.d(textContains="Connected").exists:
                self.d(textContains="Connected").click()
            elif self.d(textContains="Sign").exists:
                self.d(textContains="Sign").click()
            self.d(text="FORGET").wait.exists(timeout=1000)
            self.d(text="FORGET").click.wait()
            self.d(text="FORGET").wait.gone(timeout=1000)
        except Exception as e:
            print(e)
            raise


class WIFI(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.wifisetting()
        print("setup done")

    def reset(self):
        '''Reset all network'''
        self.common.resetnetwork()
        self.common.wifisetting()
        if self.d(textContains="Connected").exists or self.d(textContains="Sign"):
            result = False
            self.d.screenshot("reset.png")
        else:
            result = True
        self.assertTrue(result)

    def wifiSwitch(self):
        '''stress test for wifi on/off when AP disconnected'''
        self.common.switchstress()
        if self.d(text="ON").exists:
            result = True
        else:
            result = False
            self.d.screenshot("switch.png")
        self.assertTrue(result)

    def wifiConnect(self):
        '''Connect Wi-Fi AP'''
        self.common.connect("MAD-wifi", "00112233445566778899123456")
        if self.d(text="10.192.253.254").exists:
            result = True
        else:
            result = False
            self.d.screenshot("connect.png")
        self.assertTrue(result)

    def wifiSwitch2(self):
        '''stress test for wifi on/off when AP connected'''
        self.common.switchstress()
        time.sleep(30)
        if self.d(textContains="Connected").exists or self.d(textContains="Sign"):
            result = True
        else:
            result = False
            self.d.screenshot("switch2.png")
        self.assertTrue(result)

    def wifiDisconnect(self):
        '''Disnnect Wi-Fi AP'''
        self.common.forget()
        time.sleep(10)
        if self.d(textContains="Connected").exists or self.d(textContains="Sign"):
            result = False
            self.d.screenshot("disconnect.png")
        else:
            result = True
        self.assertTrue(result)

    def wifiConnect2(self):
        '''Connect another Wi-Fi AP'''
        self.common.connect("External-Internet", "1Freescale")
        if self.d(text="FORGET").exists:
            result = True
        else:
            result = False
            self.d.screenshot("Connect2.png")
        self.assertTrue(result)

    def tearDown(self):
        self.d.press.home()
        time.sleep(5)
        print("tearDown")


class BT(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.btsetting()
        print("setup done")

    def turnOn(self):
        '''test BT turn ON function'''
        if self.d(text="OFF").exists:
            self.d(text="OFF").click.wait()
            print("Turn on BT")
        time.sleep(2)
        if self.d(text="Pair new device").exists:
            result = True
        else:
            result = False
            self.d.screenshot("bton.png")
        self.assertTrue(result)

# ==================================================================================
# Author: Feiyan Wang
# Date: 6/07/2018
#
# Description:test change BT name
# ==================================================================================
    def changeBtName(self):
        try:
            if self.d(text="OFF").exists:
                self.d(text="OFF").click.wait()
                print("Turn on BT")
            time.sleep(2)
            if self.d(text="Device name").exists:
                self.d(text="Device name").click.wait()
                time.sleep(2)
                if self.d(text="Rename this device").exists:
                    Btname = "AAA"
                    self.d(resourceId="com.android.settings:id/edittext", className="android.widget.EditText").set_text(Btname)
                    time.sleep(2)
                    self.d(text="RENAME").click.wait()
                    time.sleep(2)
                    if self.d(text="Device name").exists:
                        if self.d(text=Btname).exists:
                            result = True
                        else:
                            result = False
                    else:
                        result = False
                else:
                    result = False
            else:
                result = False
            self.d.screenshot("changeBtName.png")
            self.assertTrue(result)
        except Exception as e:
            print(e)

            raise
# ==================================================================================

    def btSwitch(self):
        '''stress test BT on/off'''
        self.common.switchstress()
        time.sleep(2)
        if self.d(text="Pair new device").exists:
            result = True
        else:
            result = False
            self.d.screenshot("BT switch.png")
        self.assertTrue(result)

    def tearDown(self):
        self.d.press.home()
        time.sleep(5)
        print("tearDown")


class AirPlane(unittest.TestCase):
    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        self.common.settings()
        print("setup done")

    def airplaneON(self):
        '''test WiFi/BT status when Airplane mode ON'''
        "After previous test steps, Both WiFi and BT should be ON here"
        self.d(scrollable=True).scroll.to(text="Network & Internet")
        self.d(text="Network & Internet").click.wait()
        switch = self.d(text="Airplane mode").right(className="android.widget.Switch")
        if switch and not switch.info[u"checked"]:
            switch.click.wait()
        self.common.wifisetting()
        wifi = self.d(className="android.widget.Switch").info[u"checked"]
        self.common.btsetting()
        BT = self.d(className="android.widget.Switch").info[u"checked"]
        if not BT and not wifi:
            result = True
        else:
            result = False
        self.assertTrue(result)

    def airplaneOFF(self):
        '''test WiFi/BT status when Airplane mode OFF'''
        self.d(scrollable=True).scroll.to(text="Network & Internet")
        self.d(text="Network & Internet").click.wait()
        switch = self.d(text="Airplane mode").right(className="android.widget.Switch")
        if switch and switch.info[u"checked"]:
            switch.click.wait()
        self.common.wifisetting()
        wifi = self.d(className="android.widget.Switch").info[u"checked"]
        self.common.btsetting()
        BT = self.d(className="android.widget.Switch").info[u"checked"]
        if BT and wifi:
            result = True
        else:
            result = False
        self.assertTrue(result)

    def tearDown(self):
        self.d.press.home()
        time.sleep(5)
        print("tearDown")
