#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.
#
# Author: Faye Dong
# Created Date: 5/28/2018
#
# Description: This script is used for adb tool basic function check
# -----------------------------------------------------------------------------------------------
from common import Common
from uiautomator import Device
import os
import unittest
import time


class Adb(unittest.TestCase):
    def __init__(self, name, serial_no):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serial_no)
        self.serial_no = serial_no

        self.common = Common(self.d, self.serial_no)
        self.common.d = self.d
        self.common.serial_no = self.serial_no

    # ==================================================================
    # check adb status: offline/online
    # ==================================================================
    def testListedDevice(self):
        try:
            out = self.common.is_adb()

            for line in out.split("\n")[1:]:
                if self.serial_no in line and "offline" in line:
                    print("Device is offline: %s\n" % self.serial_no)
                    result = False
                    self.assertTrue(result)
                elif self.serial_no in line and "device" in line:
                    print("Device is online: %s\n" % self.serial_no)
                    result = True
                    self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb install application
    # ==================================================================
    def testAdbInstall(self):
        try:
            name = 'Chrome_52.0.2743.98.apk'
            package_name = 'com.android.chrome'
            r = self.common.check_installed_apk(package_name)
            if len(r) == 0:
                file_list = os.listdir('../../resource/')
                for file in file_list:
                    if file == name:
                        apk_name = os.path.join('../../resource/', name)
                        self.common.install_apk(apk_name)
                        time.sleep(5)
                        print("Successfully installed the apk.\n")
                        result = True
                        self.assertTrue(result)
                    else:
                        continue
            else:
                print("Apk already installed, please double-check!\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb uninstall application
    # ==================================================================
    def testAdbUninstall(self):
        try:
            r = self.common.check_installed_apk('chrome')
            print(r)
            if len(r) != 0:
                r = self.common.uninstall_apk(r[8:])
                time.sleep(5)
                r = r >> 8
                self.assertEqual(r, 0)
            else:
                print("The apk doesn't exist, please double-check!\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb push one file to DUT
    # ==================================================================
    def testAdbPushFile(self):
        try:
            file_name = 'LargePic300.bmp'
            src = os.path.join('../../resource/300Pic/', file_name)
            dst_path = '/mnt/sdcard/Pictures/'
            r = self.common.push_file(src, dst_path)
            if r == 0:
                t = self.common.refresh_file(dst_path + file_name)
                if len(t) != 0 and 'Broadcast completed: result=0' in t:
                    print("File has been pushed to target folder.\n")
                    result = True
                    self.assertTrue(result)
                else:
                    print("File has not been pushed to target folder.\n")
                    result = False
                    self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb pull file from DUT to PC
    # ==================================================================
    def testAdbPullFile(self):
        try:
            src = '/mnt/sdcard/Pictures/'
            dst = './'
            r = os.system(u'adb -s ' + self.serial_no + ' shell ls ' + src)
            r = r >> 8
            if int(r) == 0:
                print("Files exist, you can pull to your PC now.\n")
                t = self.common.pull_file(src, dst)
                t = t >> 8
                print(t)
                if int(t) == 0:
                    print("Successfully pull files to your PC.\n")
                    result = True
                    self.assertTrue(result)
                else:
                    print("Something wrong happened, please double-check.\n")
                    result = False
                    self.assertTrue(result)
            else:
                print("No any files, please double-check.\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb push multiple file(300 pictures) to the device
    # ==================================================================
    def testAdbPushMultiFiles(self):
        try:
            file_name = '*.jpg'
            src_path = '../../resource/300Pic/'
            src_file = os.path.join(src_path, file_name)
            dst_path = '/mnt/sdcard/Pictures/'
            r = self.common.push_file(src_file, dst_path)
            if r == 0:
                count = 0
                for root, dirs, files in os.walk(src_path):
                    for file in files:
                        if os.path.splitext(file)[1] == '.jpg':
                            t = self.common.refresh_file(dst_path + file)
                            log = 'Broadcast completed: result=0'
                            if len(t) != 0 and log in t:
                                count = count + 1
                                print('No.%s file refresh successfully.\n'
                                      % count)
                            else:
                                print("Failed to pull or refresh.\n")
                        else:
                            continue
                self.assertEqual(count, 300)
            else:
                print("Failed to push file, please double-check!\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb debug over network mode
    # ==================================================================
    def testAdbOverNetwork(self):
        try:
            ip_address = self.common.get_ip_address()
            output = self.common.is_adb()
            print(output)
            for line in output.split("\n")[1:]:
                if self.serial_no in line and "offline" in line:
                    print("Device is offline: %s\n" % self.serial_no)
                    result = False
                    self.assertTrue(result)
                elif self.serial_no in line and "device" in line:
                    print("Device is online: %s\n" % self.serial_no)
                    time.sleep(2)
                    log = self.common.restart_tcp('5555')
                    print(log)
                    time.sleep(5)
                    if 'restarting in TCP mode' in log:
                        print("Successfully restart TCP mode.\n")
                        log2 = self.common.adb_connect(ip_address)
                        if 'connected to ' + ip_address + ':5555' in log2:
                            print("Successfully connect over network.\n")
                            output2 = self.common.is_adb()
                            if ip_address + ':5555' in output2:
                                print("DUT was recognized.\n")
                                result = True
                                self.assertTrue(result)
                            else:
                                print("Can't recognize DUT.\n")
                                result = False
                                self.assertTrue(result)
                        else:
                            print("Connection failed, please check.\n")
                            result = False
                            self.assertTrue(result)
                    else:
                            print("Can't enter TCP mode, please check.\n")
                            result = False
                            self.assertTrue(result)
                else:
                    print("The device hasn't been recognized.\n")
        except Exception as e:
            print(e)
            raise

    # ==================================================================
    # check adb change back to usb debug mode
    # ==================================================================
    def testAdbOverUSB(self):
        try:
            r = self.common.adb_kill_server()
            time.sleep(5)
            r = r >> 8
            if r == 0:
                print("Successfully killed adb server.\n")
                output = self.common.is_adb()
                print(output)
                time.sleep(5)
                for line in output.split("\n")[3:]:
                    if self.serial_no in line and "device" in line:
                        print("Device is online.\n")
                        output = self.common.restart_usb()
                        print(output)
                        time.sleep(5)
                        if 'restarting in USB mode' == output:
                            print("Successfully change to USB mode.\n")
                            output2 = self.common.is_adb()
                            for i in output2.split("\n")[1:]:
                                if self.serial_no in line and "device" in i:
                                    print("Device is online.\n")
                                    result = True
                                    self.assertTrue(result)
                                elif self.serial_no in i and "offline" in i:
                                    print("Device is offline.\n")
                                    result = False
                                    self.assertTrue(result)
                                else:
                                    print("Device hasn't been recognized.\n")
                                    result = False
                                    self.assertTrue(result)
                        else:
                            print("Can't resume adb USB mode.\n")
                            result = False
                            self.assertTrue(result)
                    else:
                        print("Device is offline.\n")
                        result = False
                        self.assertTrue(result)
            else:
                print("Failed to kill adb server.\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise
