#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.
# Author: Faye Dong
# Created Date: 5/14/2018
# Description: OTA update test script
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb and uiautomator have been installed.
# 2. Please ensure the network work well before running the test scripts:
#    a) Sandard release: Use Ethernet or WiFi
#    b) Automotive release: Use WiFI
# -----------------------------------------------------------------------------------------------

import sys
import os
import time
import unittest
from uiautomator import Device
from common import Common
sys.path.append('../utils')


class OTA(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def setUp(self):
        try:
            platform = self.common.get_product_name()
            self.common.open_settings(platform)
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            platform = self.common.get_product_name()
            self.common.return_home(platform)
        except Exception as e:
            print(e)
            raise
# ===================================================================================
# test OTA upgrade function
# NOTES:
# 1. OTA upgrade & OTA diff upgrade can not run on the same date.
# 2. OTA upgrade: System update from old version to latest daily build;
# 3. OTA diff upgrade:
#    System update from yesterday's daily build to today's daily build;
# ===================================================================================

    def testOTAfromUI(self):
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            if self.d(text="Additional system updates").exists:
                self.d(text="Additional system updates").click.wait()
                time.sleep(3)
                if self.d(text="Error: needs wifi network.",
                          resourceId="com.fsl.android.ota:id/message_text"
                                     "_view").exists:
                    print ("Please check if network worked on your board.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Have a new release",
                            resourceId="com.fsl.android.ota:id/"
                                       "message_text_view").exists:
                    print ("System found a new release.\n")
                    self.d(text="Upgrade",
                           resourceId="com.fsl.android.ota:id/upgrade"
                                      "_button").click.wait()
                    time.sleep(5)
                    if self.d(text="Freescale OTA System Update "
                                   "has stopped").exists:
                        print("System update stopped!\n")
                        self.assertTrue(False)
                    else:
                        time.sleep(500)
                        os.system(u'adb -s ' + self.serialno
                                  + ' shell logcat -d | grep "update_'
                                    'engine" > Update_log.txt')
                        time.sleep(5)
                        r = os.system(u'grep -E "Update successfully applied,'
                                      u' waiting to reboot." Update_log.txt')
                        r = r >> 8
                        if r == 0:
                            self.assertTrue(True)
                        else:
                            self.assertTrue(False)

                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Error: Can not connect to server").exists:
                    print("Server doesn't work, please contact "
                          "developer team.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Your software is up to date").exists:
                    print("System update to date, please try another day.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
            else:
                print ("Can't find additional system update item.\n")
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise

    def testDiffOTAfromUI(self):
        try:
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()
            if self.d(text="Additional system updates").exists:
                self.d(text="Additional system updates").click.wait()
                time.sleep(3)
                if self.d(text="Error: needs wifi network"
                               ".", resourceId="com.fsl.android.ota:id"
                                               "/message_text_view").exists:
                    print ("Please check if network worked on your board.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Have a new release"
                                 "", resourceId="com.fsl.android.ota:id"
                                                "/message_text_view").exists:
                    print ("System found a new release.\n")
                    self.d(text="Diff Upgrade",
                           resourceId="com.fsl.android.ota:id/diff_upgrade"
                                      "_button").click.wait()
                    time.sleep(5)
                    if self.d(text="Freescale OTA System Update has"
                                   " stopped").exists:
                        print("System update stopped!\n")
                        self.assertTrue(False)
                    else:
                        time.sleep(500)
                        os.system(u'adb -s ' +
                                  self.serialno + ' shell logcat -d | grep '
                                                  '"update_engine" > Diff_'
                                                  'update_log.txt')
                        time.sleep(5)
                        r = os.system(u'grep -E "Update successfully applied,'
                                      u' waiting to reboot." Diff_'
                                      u'update_log.txt')
                        r = r >> 8
                        if r == 0:
                            self.assertTrue(True)
                        else:
                            self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Error: Can not connect to server").exists:
                    print("Server doesn't work, please contact "
                          "developer team.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
                elif self.d(text="Your software is up to date").exists:
                    print("Your system is up to date, please try "
                          "another day.\n")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.d.press.back()
            else:
                print ("Can't find additional system update item.\n")
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise
