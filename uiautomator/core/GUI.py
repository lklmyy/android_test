#! /usr/bin/env python3
#  -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.
# Author: Faye Dong
# Created Date: 3/15/2018
# Updated Date: 3/30/2018
# Updated Date: 5/07/2018
# Description: This script is used for GUI basic function check
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb and uiautomator have been installed.
# -----------------------------------------------------------------------------------------------
import sys
from uiautomator import Device
from clear import Clear
from common import Common
import os
import time
import unittest
sys.path.append('../utils')


class GUI(unittest.TestCase):
    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.clear = Clear(self.d, self.serialno)
        self.clear.d = self.d
        self.clear.serialno = self.serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self. d
        self.common.serialno = self.serialno

    def setUp(self):
        try:
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_07
# Description: Launcher rotation
# Notes: Ensure the screen is full of icons before testing,
# e.g: install some apk or copy some icons
# ***************************************************************************************************

    def testLauncherRotate(self):
        try:
            self.d.swipe(400, 400, 400, 20, steps=10)
            content = self.common.get_rotation_status()
            print(content)
            if int(content) == 1:
                self.common.set_rotation_status(0)
                time.sleep(5)
                self.common.set_rotation_value(3)
                time.sleep(2)
                result = self.common.get_rotation_value()
                self.assertEqual(int(result), 3)
                self.common.set_rotation_value(2)
                time.sleep(2)
                result = self.common.get_rotation_value()
                self.assertEqual(int(result), 2)
                self.common.set_rotation_value(1)
                time.sleep(2)
                result = self.common.get_rotation_value()
                self.assertEqual(int(result), 1)
                self.common.set_rotation_value(0)
                time.sleep(2)
                result = self.common.get_rotation_value()
                self.assertEqual(int(result), 0)
            else:
                print ("can't configure rotation status\n")
                self.assertTrue(False)
            content = self.common.get_rotation_status()
            if int(content) == 0:
                self.common.set_rotation_status(1)
                time.sleep(2)
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_03
# Description: Home Screen Shot
# Notes: Please check if the home.png display well
# although the case passed to avoid display issue
# ***************************************************************************************************

    def testHomeScreenShot(self):
        try:
            self.d.press.home()
            r = self.common.set_screen_capture("/sdcard/Pictures/home.png")
            print(r)
            r = r >> 8
            self.assertEqual(r, 0)
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_03
# Description: Live Wallpaper Screen Shot
# Notes: Please check if the cube.png display well
# although the case passed to avoid display issue
# ***************************************************************************************************

    def testLiveWallPaperScreenshot(self):
        try:
            self.d.long_click(500, 500)
            if self.d(text="WALLPAPERS").wait.exists():
                self.d(text="WALLPAPERS").click()
                time.sleep(3)
                if self.d(text="Complete action using").wait.exists():
                    self.d(text="Live Wallpapers").click()
                    time.sleep(3)
                    self.d(text="JUST ONCE").click()
                    time.sleep(3)
                    if self.d(text="Cube").wait.exists():
                        self.d(text="Cube").click()
                        time.sleep(3)
                        if self.d(text="SET WALLPAPER").wait.exists():
                            r = self.common.set_screen_capture("/sdcard"
                                                               "/Pictures"
                                                               "/Cube.png")
                            print(r)
                            r = r >> 8
                            self.assertEqual(r, 0)
                elif self.d(text="Complete action using Live Wallpapers").wait\
                        .exists():
                    self.d(text="JUST ONCE").click()
                    time.sleep(3)
                    if self.d(text="Cube").wait.exists():
                        self.d(text="Cube").click()
                        time.sleep(3)
                        if self.d(text="SET WALLPAPER").wait.exists():
                            r = self.common.set_screen_capture("/sdcard"
                                                               "/Pictures"
                                                               "/Cube.png")
                            print(r)
                            r = r >> 8
                            self.assertEqual(r, 0)
                elif self.d(text="Complete action using Gallery").wait.\
                        exists():
                    self.d(text="Live Wallpapers").click()
                    time.sleep(3)
                    if self.d(text="Cube").wait.exists():
                        self.d(text="Cube").click()
                        time.sleep(3)
                        if self.d(text="SET WALLPAPER").wait.exists():
                            r = self.common.set_screen_capture("/sdcard"
                                                               "/Pictures"
                                                               "/Cube.png")
                            print(r)
                            r = r >> 8
                            self.assertEqual(r, 0)
                else:
                    print ("Can't find live paper item!")
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_03
# Description: Settings Screen Shot
# Notes: Please check if the Settings.png display well
# although the case passed to avoid display issue
# ***************************************************************************************************

    def testSettingsScreenshot(self):
        try:
            os.system(u'adb -s '+self.serialno+' shell am start com.'
                                               'android.settings/.Settings')
            time.sleep(3)
            r = self.common.set_screen_capture("/sdcard/Pictures"
                                               "/Settings.png")
            print(r)
            r = r >> 8
            self.assertEqual(r, 0)
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_03
# Description: Video Playback Screen Shot
# Notes:
#  1. Please ensure network work OK as this case will play streaming video;
#  2. Please check if the video.png display well although the case passed
# to avoid display issue;
# ***************************************************************************************************

    def testVideoScreenshot(self):
        try:
            os.system(u'adb -s ' + self.serialno + ' shell am start -a '
                                                   'android.intent.action.'
                                                   'VIEW -t video/* -d '
                                                   'http://10.192.225.205/'
                                                   'streaming/http/H264_BP11'
                                                   '_352x288_25_1536'
                                                   '_AAC_11_64_1_friendsr'
                                                   '.mp4')
            time.sleep(10)
            if self.d(className='android.widget.TextView').wait.exists():
                print("Please check if network work OK!")
                self.assertTrue(False)
            else:
                r = self.common.set_screen_capture("/sdcard/Pictures"
                                                   "/Video.png")
                print(r)
                r = r >> 8
                self.assertEqual(r, 0)
            self.d.press.back()
        except Exception as e:
            print(e)
            raise
# ***************************************************************************************************
# Case ID: Android_GUI_03
# Description: Sound Recorder Screen Shot
# Notes: Please check if the Record.png display well
# although the case passed to avoid display issue;
# ***************************************************************************************************

    def testRecorderScreenshot(self):
        try:
            os.system(u'adb -s '+self.serialno+' shell am start '
                                               'com.android.soundrecorder'
                                               '/.SoundRecorder')
            time.sleep(3)
            if self.d(text="ALLOW").exists:
                self.d(text="ALLOW").click.wait()
                time.sleep(3)
                if self.d(text="ALLOW").exists:
                    self.d(text="ALLOW").click.wait()
                    time.sleep(3)
                    print ("Open sound recorder successfully")
            if self.d(text="Record your message").wait.exists():
                self.d(resourceId="com.android.soundrecorder:id/"
                                  "recordButton").click()
                time.sleep(5)
                r = self.common.set_screen_capture("/sdcard/Pictures"
                                                   "/Record.png")
                print(r)
                r = r >> 8
                self.assertEqual(r, 0)
                if self.d(text="Recording").wait.exists():
                    self.d(resourceId="com.android.soundrecorder:id/"
                                      "stopButton").click()
                    time.sleep(2)
                    if self.d(text="Message recorded").wait.exists():
                        self.d(resourceId="com.android.soundrecorder:id/"
                                          "discardButton").click()
                        time.sleep(2)
        except Exception as e:
            print(e)
            raise
