#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc..
#
# Author: Faye Dong
# Created date: 3/01/2018
# Updated date: 6/18/2018
#
# Description: Android SoundRecorder app UI test script
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb and uiautomator installed before testing;
# 2. Please ensure MIC work well on the test board;
# 3. Connect an audio cable between DUT and the PC and play an audio;
# 4. Filter sound recorder related error log to judge initial pass/fail;
# -----------------------------------------------------------------------------------------------
from clear import Clear
import os
import time
import unittest
from common import Common
from uiautomator import Device


class Prepare(unittest.TestCase):
    def __init__(self, name, serial_no):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serial_no)
        self.serial_no = serial_no

        self.serial_no = serial_no
        self.clear = Clear(self.d, self.serial_no)
        self.clear.d = self.d
        self.clear.serial_no = self.serial_no

    def delete_old_recording_file(self):
        try:
            cmd = u'adb -s ' + self.serial_no \
                 + ' shell am start com.android.music/.MusicBrowserActivity'
            os.system(cmd)
            if self.d(resourceId="com.android.music:id/playlisttab")\
                    .wait.exists():
                self.d(resourceId="com.android.music:id/playlisttab").click()
                print("Navigate to playlist tab.\n")
                if self.d(text="My recordings").wait.exists():
                    self.d(text="My recordings").long_click()
                    time.sleep(2)
                    if self.d(text="Delete").wait.exists():
                        self.d(text="Delete").click()
                    else:
                        print("Can't find delete item.\n")
                else:
                    print("No old recording file.\n")
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise


class SoundRecorderTest(unittest.TestCase):
    def __init__(self, name, serial_no):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serial_no)
        self.serial_no = serial_no
        self.name = name

        self.serial_no = serial_no
        self.common = Common(self.d, self.serial_no)
        self.common.d = self.d
        self.common.serial_no = self.serial_no
        self.clear = Clear(self.d, self.serial_no)
        self.clear.d = self.d
        self.clear.serial_no = self.serial_no

    def setUp(self):
        try:
            platform = self.common.get_product_name()
            if platform == 'mek_8q_car':
                self.common.install_sound_recorder()
            self.clear.clearAll()
            cmd = u'adb -s '\
                  + self.serial_no \
                  + ' shell am start com.android.soundrecorder/.SoundRecorder'
            os.system(cmd)
            time.sleep(2)
            # Allow using sound recorder app
            if self.d(text="ALLOW").exists:
                self.d(text="ALLOW").click.wait()
                time.sleep(3)
                if self.d(text="ALLOW").exists:
                    self.d(text="ALLOW").click.wait()
                    time.sleep(3)
                    print("Open sound recorder successfully.\n")
                else:
                    print("Not find 2nd ALLOW window.\n")
            elif self.d(text="Record your message").exists:
                print("Open sound recorder successfully.\n")
            else:
                print("Not find ALLOW window.\n")
        except Exception as e:
            print(e)
            raise

    def testDiscard(self):
        try:
            # Check recording file number before testing
            cmd = u'adb shell ls /mnt/sdcard | grep recording*'
            r = os.popen(cmd)
            original_file_no = len(r.readlines())
            r.close()
            # Start recording
            if self.d(text="Record your message").wait.exists():
                self.d(resourceId="com.android.soundrecorder:id/recordButton")\
                    .click()
                print("Start recording...\n")
                time.sleep(5)
                # Stop recording and discard
                if self.d(text="Recording").wait.exists():
                    self.d(resourceId="com.android.soundrecorder:id/stopButton")\
                        .click()
                    print("Stop recording...\n")
                    time.sleep(2)
                    # Check if new record file exist
                    r = os.popen(cmd)
                    current_file_no = len(r.readlines())
                    r.close()
                    self.assertEqual(current_file_no - original_file_no, 1)
                    if self.d(text="Message recorded").wait.exists():
                        self.d(resourceId="com.android.soundrecorder:id/discardButton")\
                            .click()
                        print("Discard the new file.\n")
                        time.sleep(2)
                        # Check again if new record file was removed
                        r = os.popen(cmd)
                        current_file_no = len(r.readlines())
                        r.close()
                        self.assertEqual(original_file_no, current_file_no)
                    else:
                        print("No new file exists.\n")
                        result = False
                        self.assertTrue(result)
                else:
                    print("Can't find stop button, please check.\n")
                    result = False
                    self.assertTrue(result)
            else:
                print("Can't find record button.\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    def testAccept(self):
        try:
            # Check recorded file number before testing
            cmd = u'adb -s ' + self.serial_no \
                  + ' shell ls /mnt/sdcard | grep recording*'
            r = os.popen(cmd)
            original_file_no = len(r.readlines())
            r.close()
            # Start recording
            if self.d(text="Record your message").wait.exists():
                self.d(resourceId="com.android.soundrecorder:id/recordButton")\
                    .click()
                print("Start recording...\n")
                time.sleep(5)
                # Stop recording
                if self.d(text="Recording").wait.exists():
                    self.d(resourceId="com.android.soundrecorder:id/stopButton")\
                        .click()
                    print("Stop recording...\n")
                    # Check if new record file exist
                    r = os.popen(cmd)
                    current_file_no = len(r.readlines())
                    r.close()
                    self.assertEqual(current_file_no - original_file_no, 1)
                    # Click "Accept" button
                    if self.d(text="Message recorded").wait.exists():
                        self.d(resourceId="com.android.soundrecorder:id/acceptButton")\
                            .click()
                        print("Accept the new file")
                        # Check again if new file exist
                        r = os.popen(cmd)
                        current_file_no = len(r.readlines())
                        r.close()
                        self.assertEqual(current_file_no - original_file_no, 1)
                    else:
                        print("No new file exists")
                        result = False
                        self.assertTrue(result)
                        time.sleep(2)
                else:
                    print("Can't find stop button, please check.\n")
                    result = False
                    self.assertTrue(result)
                    time.sleep(2)
            else:
                print("Can't find record button.\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    def testPlay(self):
        try:
            # Start recording
            if self.d(text="Record your message").wait.exists():
                self.d(resourceId="com.android.soundrecorder:id/recordButton")\
                    .click()
                print("Start recording...\n")
                time.sleep(50)
                # Stop recording
                if self.d(text="Recording").wait.exists():
                    self.d(resourceId="com.android.soundrecorder:id/stopButton")\
                        .click()
                    print("Stop recording...\n")
                    time.sleep(2)
                    os.system(u'adb -s ' + self.serial_no + ' shell logcat -c')
                    time.sleep(2)
                    # Play the recorded file by SoundRecorder
                    if self.d(text="Message recorded").wait.exists():
                        self.d(resourceId="com.android.soundrecorder:id/playButton")\
                            .click()
                        print("Playing...")
                        time.sleep(52)
                        # Check the log
                        print("Check the log")
                        platform = self.common.get_product_name()
                        if platform == 'mek_8q_car':
                            r = os.system(u'adb -s ' + self.serial_no + ' shell logcat -d *:E | grep -vE "beginning|FslExtractor|FslInspector|OMXNodeInstance|TaskPersister|MetadataRetrieverClient|cutils-trace"')
                        elif platform == 'mek_8q':
                            r = os.system(u'adb -s ' + self.serial_no + ' shell logcat -d *:E | grep -vE "*beginning of|FslExtractor|FslInspector|OMXNodeInstance|TaskPersister|MetadataRetrieverClient"')
                        r = r >> 8
                        print(r)
                        self.assertEqual(r, 1)
                        # Accept the new recorded file
                        if self.d(text="Message recorded").wait.exists():
                            self.d(resourceId="com.android.soundrecorder:id/acceptButton").click()
                            time.sleep(2)
                            # Open Music app
                            cmd = u'adb -s ' + self.serial_no + ' shell am start com.android.music/.MusicBrowserActivity'
                            os.system(cmd)
                            time.sleep(2)
                            if self.d(resourceId="com.android.music:id/playlisttab").wait.exists():
                                self.d(resourceId="com.android.music:id/playlisttab").click()
                                print("Navigate to playlist tab")
                                if self.d(text="My recordings").wait.exists():
                                    self.d(text="My recordings").click()
                                    time.sleep(2)
                                    os.system('adb shell logcat -c')
                                    time.sleep(2)
                                    # Play the recorded file by Music app
                                    self.d(className="android.widget.RelativeLayout").child(text="Your recordings", className="android.widget.TextView", resourceId="com.android.music:id/line2").click()
                                    time.sleep(10)
                                    # Pause
                                    cmd = 'adb -s ' + self.serial_no + ' shell input keyevent KEYCODE_MEDIA_PAUSE'
                                    os.system(cmd)
                                    time.sleep(2)
                                    # Seek to the center
                                    self.d(className="android.widget.SeekBar")\
                                        .click()
                                    time.sleep(2)
                                    # Play
                                    cmd = 'adb -s ' + self.serial_no \
                                          + ' shell input keyevent KEYCODE_MEDIA_PLAY'
                                    os.system(cmd)
                                    time.sleep(10)
                                    # Seek to the end while playing
                                    self.d(className="android.widget.SeekBar")\
                                        .click.bottomright()
                                    time.sleep(2)
                                    # Check the log
                                    platform = self.common.get_product_name()
                                    if platform == 'mek_8q_car':
                                        r = os.system('adb -s ' + self.serial_no + ' shell logcat -d *:E | grep -vE "beginning|FslExtractor|FslInspector|OMXNodeInstance|TaskPersister|MetadataRetrieverClient|cutils-trace"')
                                    elif platform == 'mek_8q':
                                        r = os.system(u'adb -s ' + self.serial_no + ' shell logcat -d *:E | grep -vE "*beginning of|FslExtractor|FslInspector|OMXNodeInstance|TaskPersister|MetadataRetrieverClient"')
                                    r = r >> 8
                                    print(r)
                                    self.assertEqual(r, 1)
                        else:
                            print("No new file exists.\n")
                            result = False
                            self.assertTrue(result)
                    else:
                        print("Can't find play button.\n")
                        result = False
                        self.assertTrue(result)
                else:
                    print("Can't find stop button.\n")
                    result = False
                    self.assertTrue(result)
            else:
                print("Can't find record button.\n")
                result = False
                self.assertTrue(result)
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.clear.clearAll()
        except Exception as e:
            print(e)
            raise
