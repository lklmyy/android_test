#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Fiona Ye
# Created Date: 6/21/2018
#
# Description: Android CODEC Enocoder cases
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure run it on a clean board environment;
# 2. Please ensure the PC you used has installed python3, uiautomator and MediaInfo;
# 3. Please ensure all cameras connected to the test board;
# 4. All recorded video files and media_profiles.xml will be backed up in ../result/testEncode.
# -----------------------------------------------------------------------------------------------

from common import Common
from uiautomator import Device
from pymediainfo import MediaInfo
import os
import time
import unittest


class EncodeCommon:
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno
        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

    def operate_camera(self):
        self.common.open_camera()
        self.common.change_function("Video")
        self.common.change_to_back()
        self.common.switch_quality("Back camera video", "HD 720p")

    def remount(self, purpose):
        try:
            if purpose == "prepare":
                os.system(u'adb -s ' + self.serialno + ' root')
                time.sleep(1)
                os.system(u'adb -s ' + self.serialno + ' disable-verity')
                time.sleep(1)
                os.system(u'adb -s ' + self.serialno + ' reboot')
                time.sleep(60)
            os.system(u'adb -s ' + self.serialno + ' root')
            time.sleep(2)
            r = os.popen(u'adb -s ' + self.serialno + ' remount')
            result = r.readlines()[0].strip('\n')
            r.close()
            if result == "remount succeeded":
                assert True
            else:
                assert False
        except Exception as e:
            print(e)
            raise

    def edit_xml(self, video_format, audio_format, file_format):
        try:
            old = '''       <EncoderProfile quality="720p" fileFormat="mp4" duration="30">
            <Video codec="h264"
                   bitRate="3000000"
                   width="1280"
                   height="720"
                   frameRate="30" />

            <Audio codec="aac"
                   bitRate="96000"
                   sampleRate="44100"
                   channels="1" />

            <Camera previewFrameRate="30" />'''
            new = '''       <EncoderProfile quality="720p" fileFormat="''' + file_format + '''" duration="30">
            <Video codec="''' + video_format + '''"
                   bitRate="3000000"
                   width="1280"
                   height="720"
                   frameRate="30" />

            <Audio codec="''' + audio_format + '''"
                   bitRate="96000"
                   sampleRate="44100"
                   channels="1" />

            <Camera previewFrameRate="30" />'''
            oldfile = open("media_profiles_backup.xml", "r", encoding="utf-8")
            oldcon = oldfile.read()
            newcon = oldcon.replace(old, new)   # edit encode format
            oldfile.close()
            newfile = open("media_profiles.xml", "w", encoding="utf-8")
            newfile.write(newcon)
            time.sleep(2)
        except Exception as e:
            print(e)
            raise

    def push_xml(self):
        try:
            time.sleep(2)
            os.system(u'adb -s ' + self.serialno + ' shell rm /system/etc/media_profiles.xml')
            time.sleep(1)
            os.system(u'adb -s ' + self.serialno + ' push media_profiles.xml /system/etc')
            time.sleep(1)
            os.system(u'adb -s ' + self.serialno + ' reboot')
            time.sleep(60)
        except Exception as e:
            print(e)
            raise

    def check_mediainfo(self, video, audiof, audiop, general):
        try:
            os.system(u'adb -s ' + self.serialno + ' shell rm -r /mnt/sdcard/DCIM/Camera')
            time.sleep(2)
            r = os.system(u'adb -s ' + self.serialno + ' shell ls /mnt/sdcard/DCIM/Camera')
            r = r >> 8
            if r == 1:
                assert True
            else:
                assert False     # ensure there is no more camera media files
            self.common.record_shoot(1, 10, general+" "+video+" "+audiof+" "+audiop+"_recording")

            cmd = u'adb -s ' + self.serialno + ' shell ls /mnt/sdcard/DCIM/Camera | grep ^VID'
            r = os.popen(cmd)
            videoname = r.readlines()[0].strip('\n')
            r.close()
            print(videoname)
            os.system(u'adb -s ' + self.serialno + ' pull /mnt/sdcard/DCIM/Camera/' + videoname)
            time.sleep(2)

            media_info = MediaInfo.parse(videoname)
            for track in media_info.tracks:
                if track.track_type == 'Video':
                    video_format = track.format
                if track.track_type == 'Audio':
                    audio_format = track.format
                    Formatprofile = track.format_profile
            Fileformat = videoname[len(videoname) - 3:len(videoname)]
            if video_format == video and audio_format == audiof and Formatprofile == audiop and Fileformat == general:
                print(video_format, audio_format, Formatprofile, Fileformat)
                assert True
            else:
                assert False    # check encode format
        except Exception as e:
            print(e)
            raise


class EncoderTest(unittest.TestCase):

    def __init__(self, name, serialno):

        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.encodecommon = EncodeCommon(self.d, self.serialno)
        self.encodecommon.d = self.d
        self.encodecommon.serialno = self.serialno

    # ***************************************************************************************************
    # Description: disable verity and remount/ pull origin media_profiles.xml and back up
    # ***************************************************************************************************
    def prepare(self):
        try:
            self.encodecommon.remount("prepare")   # disable verity
            os.system(u'adb -s ' + self.serialno + ' pull /system/etc/media_profiles.xml')  # pull origin media_profile.xml to result/testEncode file
            time.sleep(1)
            os.system(u'cp -f media_profiles.xml media_profiles_backup.xml')  # back up media_profiles.xml
            cmd = u'ls | grep ^media_profiles'
            r = os.popen(cmd)
            filenum = len(r.readlines())
            r.close()
        except Exception as e:
            print(e)
            raise
        self.assertEqual(filenum, 2)       # check pull and back up result

    # ***************************************************************************************************
    # Description: origin encoder is "h264","aac","mp4" so just record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H264AAC(self):
        # self.encodecommon.remount("execution")
        # self.encodecommon.edit_xml("h264","aac","mp4")
        # self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AAC", "LC",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h264","aac","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H264AAC(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h264", "aac", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AAC", "LC",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "h264","amrnb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H264AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h264", "amrnb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AMR", "Narrow band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h264","amrnb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H264AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h264", "amrnb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AMR", "Narrow band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "h264","amrwb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H264AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h264", "amrwb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AMR", "Wide band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h264","amrwb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H264AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h264", "amrwb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("AVC",
                                          "AMR", "Wide band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "h263","aac","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H263AAC(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "aac", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AAC", "LC",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h263","aac","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H263AAC(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "aac", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AAC", "LC",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "h263","amrnb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H263AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "amrnb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AMR", "Narrow band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h263","amrnb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H263AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "amrnb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AMR", "Narrow band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "h263","amrwb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_H263AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "amrwb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AMR", "Wide band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "h263","amrwb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_H263AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("h263", "amrwb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("H.263",
                                          "AMR", "Wide band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","aac","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_MPEG4AAC(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "aac", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AAC", "LC",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","aac","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_MPEG4AAC(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "aac", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AAC", "LC",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","amrnb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_MPEG4AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "amrnb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AMR", "Narrow band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","amrnb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_MPEG4AMRNB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "amrnb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AMR", "Narrow band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","amrwb","mp4" and record a video then check video's encoded format
    # ***************************************************************************************************
    def testMP4_MPEG4AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "amrwb", "mp4")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AMR", "Wide band",
                                          "mp4")

    # ***************************************************************************************************
    # Description: change encoder to "m4v","amrwb","3gp" and record a video then check video's encoded format
    # ***************************************************************************************************
    def test3GP_MPEG4AMRWB(self):
        self.encodecommon.remount("execution")
        self.encodecommon.edit_xml("m4v", "amrwb", "3gp")
        self.encodecommon.push_xml()
        self.encodecommon.operate_camera()
        self.encodecommon.check_mediainfo("MPEG-4 Visual",
                                          "AMR", "Wide band",
                                          "3gp")

    # ***************************************************************************************************
    # Description: restore media_profiles.xml to origin version then push it into system/etc
    # ***************************************************************************************************
    def restore(self):
        try:
            time.sleep(1)
            os.system(u'cp -f '
                      u'media_profiles_backup.xml '
                      u'media_profiles.xml')  # restore media_profiles.xml
            self.encodecommon.remount("execution")
            self.encodecommon.push_xml()
        except Exception as e:
            print(e)
            raise

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)
