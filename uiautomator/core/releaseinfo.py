#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# -----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Faye Dong
# Created Date: 5/14/2018
#
# Description: Use this script to check release related info 
# -----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure python3, pyusb and uiautomator have been installed on the Ubuntu OS PC you used;
# -----------------------------------------------------------------------------------------------
import os, time, unittest
from uiautomator import Device
from common import Common

class ReleaseInfo(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno

        self.common = Common(self.d, self.serialno)
        self.common.d = self.d
        self.common.serialno = self.serialno

#===================================================================================
# check CPU frequency with special user case
# NOTE:
# 1) Please ensure network work well before run the test; 
#===================================================================================
#    Comments this function as Benchmark & Performance test team have covered CPU loading test during CPU & Bus frequency test item
#    def testCPUfrequency1(self):
#        try:
#            r = os.system(u'adb -s ' + self.serialno + ' shell top -d 20 -n 1 | grep -E "mediaserver"')
#            r = r>>8
#            print(r)
#            if r == 1:
#                print("No other application use audio or media server.\n")
#                os.system(u'adb -s ' + self.serialno + ' shell am start -a android.intent.action.VIEW -t audio/*  -d http://10.192.225.205/streaming/http/Mpeg1L2_44kHz_128kbps_m_MoonMiss.mp3')
#                time.sleep(2)
#                if self.d(text="Open with", resourceId="android:id/title").exists:
#                    self.d(text="Music").click()
#                    time.sleep(2)
#                    self.d(text="JUST ONCE").click()
#                    time.sleep(2)
#                elif self.d(text="Open with Music", resourceId="android:id/title").exists:
#                    self.d(text="JUST ONCE").click()
#                    time.sleep(2)
                          
#                r = os.system(u'adb -s ' + self.serialno + ' shell top -d 20 -n 1 | grep -E "mediaserver"')
#                r = r>>8
#                self.assertEqual(r, 0)
#                  
#                self.d.press.back()
#                time.sleep(3)

#                r = os.system(u'adb -s ' + self.serialno + ' shell top -d 20 -n 1 | grep -E "mediaserver"')
#                r = r>>8
#                self.assertEqual(r, 1)
#            else:
#                print("Please check if you opened other app to call media server!\n")
#                self.assertTrue(False)          
#        except Exception as e:
#            print(e)
#            raise
#===================================================================================
# check U-boot version
#===================================================================================
    def testUbootVersion(self):
        try:
            platform = os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.product.name').read().strip()
            #platform = self.common.getProductName()
            if platform == "mek_8q" or platform == "mek_8q_car":
                r = os.system(u'adb -s ' + self.serialno + ' shell su root dd if=/dev/block/mmcblk0boot0 of=/mnt/uboot.qm bs=1k seek=0 count=2048')
            elif platform == "sabresd_6dq":
                r = os.system(u'adb -s ' + self.serialno + ' shell su root dd if=/dev/block/mmcblk3boot0 of=/mnt/uboot.qm bs=1k seek=0 count=2048')
            else:
                print("Please check the mmcblk info for the board you used!\n")
            print(r)
            if int(r) == 0:
                print("Get uboot version succesffully!\n")
                version = input('Please input correct Uboot version(e.g:2017.03):')
                t = os.system(u'adb -s ' + self.serialno + ' shell su root grep '+ str(version) + ' /mnt/uboot.qm')
                t = t>>8
                if int(t) == 0:
                    print("uboot version is correct.\n")
                    self.assertEqual(t, 0)
                else:
                    print("uboot version is incorrect.\n")
                    self.assertTrue(False)
            else:
               print("Cannot get uboot version")
               self.assertTrue(False)

                #version = input('Please input correct Uboot version(e.g:2017.03):')
                #t = os.system(u'adb -s ' + self.serialno + ' shell su root grep '+ str(version) + ' /mnt/uboot.qm')
                #t = t>>8
                #print("uboot version is correct.\n")
                #self.assertEqual(t, 0) 
           
                #print("uboot version is incorrect.\n")
                #self.assertTrue(False)
        except Exception as e:
            print(e)
            raise
#===================================================================================
# check Android version
#===================================================================================
    def testAndroidVersion(self):
        try:
            platform = self.common.getProductName()
            self.common.openSettings(platform)
            self.d(scrollable=True).scroll.to(text="System")
            self.d(text="System").click.wait()    
            self.d(text="About tablet").click.wait()  
            #version = input('Please input current Android version(e.g: 7.0.0):')
            version = os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.build.version.release').read().strip()
            if self.d(index="4").child(text=version).exists:
                print("Android version is correct.\n")
                self.assertTrue(True)
            else:
                print("Android version is incorrect, please check it.\n")
                self.assertTrue(False)

            self.d.press.back()
            self.d.press.back()
            self.common.returnHome(platform) 
        except Exception as e:
            print(e)
            raise
#===================================================================================
# check Kernel version
#===================================================================================
    def testKernelVersion(self):
        try:
            version = os.popen(u'adb -s ' + self.serialno + ' shell cat /proc/version').read().strip()[14:20]
            print("Kernel version is: %s\n" % version)
            kernel = input("Please input Kernel version(e.g:4.9.69):")
            if kernel == version:
                print("Kernel version is correct.\n")
                self.assertTrue(True)
            else:
                print("Kernel version is incorrect, please check it.\n")
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise
#===================================================================================
# check GPU version
# NOTE:
# There are 2 ways to input GPU version:
#     a) Input version info by interaction with user: use input() function; --Semi-auto
#     b) Directly modify code and change to the value R&D team offered, e.g: V6.2.4; --Auto
#===================================================================================
    def testGPUVersion(self):
        try:
            r = os.popen(u'adb -s ' + self.serialno + ' shell logcat -d | grep -E "SurfaceFlinger: version   : OpenGL ES"').read().strip()
            version = r.split()[11]
            print(version)
            val = input("Please input current GPU version:")
            print(val)
            if val in version:
            #if "V6.2.4" in version:
                self.assertTrue(True)         
            else:
                print("Current GPU version is not aligned or can't get related log.\n")
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise
#===================================================================================
# test Selinux mode function
#===================================================================================
    def testEnforceMode(self):
        try:
            mode = os.popen(u'adb -s ' + self.serialno + ' shell getenforce').read().strip()
            print(mode)
            if mode == 'Enforcing':
                print ("Current mode is enforce mode.\n")
                r = os.system(u'adb -s ' + self.serialno + ' shell su root dmesg | grep -E "avc:"')
                r = r>>8
                self.assertEqual(r, 0)
            else:
                print ("Current mode is not enforce mode.\n")
                assertTrue(False)
        except Exception as e:
            print(e)
            raise    
#===================================================================================
# test File based Encryption function
#===================================================================================
    def testEncryptionFromUI(self):
        try:
            platform = self.common.getProductName()
            self.common.openSettings(platform)
            if self.d(text="Security & location").exists:
                self.d(text="Security & location").click.wait()
                if self.d(text="Encryption & credentials").exists:
                    self.d(text="Encryption & credentials").click.wait()
                    if self.d(text="Encrypted", resourceId="android:id/summary").wait.exists():
                        time.sleep(3)
                        self.assertTrue(True)
                        self.d.press.back()
                        self.d.press.back()
                        self.common.returnHome(platform)
                else:
                    print ("Not find Encryption & credentials item")
                    self.assertTrue(False)
                    self.d.press.back()
                    self.common.returnHome(platform)
            else:
                print ("Not find security & location item")
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise    
            
    def testEncryptionByCommand(self):
        try:
            result = os.popen(u'adb -s ' + self.serialno + ' shell getprop ro.crypto.type').read().strip()
            print(result)
            if result == 'file':
                self.assertTrue(True)
            else:
                self.assertTrue(False)
        except Exception as e:
            print(e)
            raise
