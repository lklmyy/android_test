#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#-----------------------------------------------------------------------------------------------
# Copyright 2018 NXP Semiconductor Inc.,
# All modifications are confidential and proprietary information of NXP Semiconductor,Inc. ALL RIGHTS RESERVED.
#
# Author: Fiona Ye
# Created Date: 3/23/2018
# Updated Date: 5/31/2018
#
# Description: Android Camera app UI test script
#-----------------------------------------------------------------------------------------------
# NOTES:
# 1. Please ensure run it on a clean board environment;
# 2. Please ensure the PC you used has installed python3, uiautomator;
# 3. Please ensure all cameras connected to the test board;
# 4. Please check the screenshots after run pass;
# 5. Result should only be OK or Fail if testing on supported boards.
#-----------------------------------------------------------------------------------------------

from clear import Clear
import HTMLTestRunner
import os
import time
import sys
import unittest

from uiautomator import Device 
        
class CameraCommon:
    def __init__(self, d, serialno):
        self.d = d
        self.serialno = serialno     


    def change_function(self,on):
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/preview_content").swipe.right(steps=10)
            self.d(text=on).click.wait()
        except Exception as e:
            print(e)
            raise

    def switch_quality(self,direc,quali):
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/preview_content").wait.exists(timeout = 1000)
            self.d(resourceId="com.android.camera2:id/preview_content").swipe.right(steps=10)
            self.d(resourceId="com.android.camera2:id/settings_button").click()
            self.d(text="Resolution & quality").click.wait()
            if self.d(text=direc).exists:
                self.d(text=direc).click()   #try to open front/back picture/video change page
            else:
                print ("can't find this direction")
                raise
            if self.d(text=quali).exists:
                self.d(text=quali).click()   #try to change quality
            else:
                print ("can't find this quality")
                raise
            self.d.press.back()
            self.d.press.back()
            time.sleep(1)
        except Exception as e:
            print(e)
            raise
            
    def record_shoot(self,RS,T,describe):         #if SR=1 it's recording, else it's taking picture
        try:
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/shutter_button").wait.exists(timeout = 1000)           
            self.d(resourceId="com.android.camera2:id/shutter_button").click()
            if RS == 1 :
                time.sleep(2)
                self.d.screenshot(describe+".png")
                time.sleep(T)        #recording for Ts
                self.d(resourceId="com.android.camera2:id/shutter_button").click.wait()         #stop recording
            time.sleep(1)
        except Exception as e:
            print(e)
            raise

    def checkpreview(self,direction,quality,O):
        self.switch_quality(direction,quality)
        time.sleep(1)
        self.d.screenshot(O+" "+direction+" "+quality+"_preview.png")
        try:
            self.d(resourceId="com.android.camera2:id/preview_content").wait.exists(timeout = 1000)
            self.d(resourceId="com.android.camera2:id/preview_content").swipe.right(steps=10)
            self.d(resourceId="com.android.camera2:id/settings_button").click()
            self.d(text="Resolution & quality").click.wait()
            self.d(text=direction).click()
        except Exception as e:
            print(e)
            raise

    def checkquality(self):   
        try:
            self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").click.wait()
            self.d(description="More options").click.wait()
            self.d(text="Details").click.wait()
            self.d(textContains="Width").wait.exists()
        except Exception as e:
            print(e)
            raise

    def checkplayback(self,Q,orientation):
        try:
            self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").click.wait()
            time.sleep(1)
            self.d(resourceId="com.android.camera2:id/play_button").click.wait()
            if self.d(textContains="Video player").wait.exists():
                self.d(textContains="Video player").click.wait()
                self.d(text="ALWAYS").click.wait()
            time.sleep(4)         #playing for 4s
            self.d.click(512, 360)
            self.d.screenshot(orientation+" "+Q+"_playback.png")
            self.d.press.back()
            self.d.press.back()
        except Exception as e:
            print(e)
            raise

    def checkpicture(self,M,Orientation):
        try:
            self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").click.wait()
            time.sleep(1)
            self.d.screenshot(Orientation+" "+M+"_play.png")
        except Exception as e:
            print(e)
            raise

    def rotate(self,aim):
        try:
            self.d.press.back()
            os.system(u'adb -s ' + self.serialno + ' shell settings put system user_rotation ' + aim)
            time.sleep(1)
            self.d(text="Camera").click.wait()
            time.sleep(3)
        except Exception as e:
            print(e)
            raise

    def opencamera(self):
        try:
            self.d.wakeup()
            self.d.press.home()
            self.d.orientation = "n"   #rotate to natural
            self.d(text="Camera").click.wait()
            time.sleep(2)
            if self.d(text="ALLOW").wait.exists(timeout = 2000):
                self.d(text="ALLOW").click.wait()
                time.sleep(2)
                if self.d(text="NEXT").wait.exists(timeout = 2000):
                    self.d(text="NEXT").click.wait()
                    time.sleep(2)
                    print("Open camera successfully")
                else:
                    print("Not find NEXT window")
            else:
                print("Not find ALLOW window")
            time.sleep(1)
        except Exception as e:
            print(e)
            raise

class Prepare(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.clear = Clear(self.d, self.serialno)
        self.clear.d = self.d
        self.clear.serialno = self.serialno

    def preparation(self):
        self.clear.clearAll()
        self.clear.clearGallery()


class Backcamcorder(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cameracommon = CameraCommon(self.d, self.serialno)
        self.cameracommon.d = self.d
        self.cameracommon.serialno = self.serialno
        
    def setUp(self):
        self.cameracommon.opencamera()
        print ("setup done")

        
#***************************************************************************************************
# Description: Open back Camera and switch to Camcorder
#***************************************************************************************************
    def testChangetoBackCamcorder(self):
        self.cameracommon.change_function("Video")
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle").click()   #open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:   #check if there is change option, change option exists that means there are 2 cameras
                second = True
                if self.d(description="Front camera").exists:                                     
                    self.d(description="Front camera").click.wait()   #set to aim direction
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                second = False                      #there is only back camera no need to check change result
        except Exception as e:
            print(e)
            raise
        if second == True:    #only when there are 2 cameras to check the change result
            if self.d(description="Back camera").exists:      #check change result
                result = True
            else:
                result = False
            expected_result = True
            self.assertEqual(result, expected_result)
            print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")
            
#***************************************************************************************************
# Description: Check back camcorder 1080p preview in natural orientation.
#***************************************************************************************************
    def testBackCamcorder1080preview(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 1080p","Natural")
        if self.d(text="HD 1080p", checked="true").exists:
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Record a 1080P video file by back camcorder to check if the record quality is right.
#***************************************************************************************************
    def testBackCamcorder1080quality(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 1080p")
        self.cameracommon.record_shoot(1,3,"Natural_Back_1080p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkquality()
            if(self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 1920 Height: 1080"):            #check if the recorded video quality is correct
                result = True
                print (" quality is correct")
            else:
                result = False
                print (" quality is incorrect")
            expected_result = True
            self.assertEqual(result, expected_result)
        else:
            assert False

#***************************************************************************************************
# Description: Record a 1080P video file by back camcorder to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamcorder1080playback(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 1080p")
        self.cameracommon.record_shoot(1,10,"Natural_Back_1080p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_1080p","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcorder 720p preview in natural orientation.
#***************************************************************************************************
    def testBackCamcorder720preview(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 720p","Natural")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Record a 720P video file by back camcorder to check if the record quality is right.
#***************************************************************************************************
    def testBackCamcorder720quality(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 720p")
        self.cameracommon.record_shoot(1,3,"Natural_Back_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkquality()
            if (self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 1280 Height: 720" or
                self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 1280 Height: 800"):
                result = True
                print (" quality is correct")
            else:
                result = False
                print (" quality is incorrect")
            expected_result = True
            self.assertEqual(result, expected_result)
        else:
            assert False

#***************************************************************************************************
# Description: Record a 720P video file by back camcorder to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamcorder720playback(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Natural_Back_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_720p","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcorder 480p preview in natural orientation.
#***************************************************************************************************
    def testBackCamcorder480preview(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","SD 480p","Natural")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by back camcorder to check if the record quality is right.
#***************************************************************************************************
    def testBackCamcorder480quality(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","SD 480p")
        self.cameracommon.record_shoot(1,3,"Natural_Back_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkquality()
            if (self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 720 Height: 480"):
                result = True
                print (" quality is correct")
            else:
                result = False
                print (" quality is incorrect")
            expected_result = True
            self.assertEqual(result, expected_result)
        else:
            assert False

#***************************************************************************************************
# Description: Record a 480P video file by back camcorder to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamcorder480playback(self):
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Natural_Back_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_480p","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check back camcorder 1080p preview in left orientation.
#***************************************************************************************************
    def testBackCamcorder1080previewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 1080p","Left")
        if self.d(text="HD 1080p", checked="true").exists:
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 1080P video file by back camcorder to check playback in left orientation.
#***************************************************************************************************
    def testBackCamcorder1080playLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 1080p")
        self.cameracommon.record_shoot(1,10,"Left_Back_1080p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_1080p","Left")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check back camcorder 720p preview in left orientation.
#***************************************************************************************************
    def testBackCamcorder720previewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 720p","Left")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 720P video file by back camcorder to check playback in left orientation.
#***************************************************************************************************
    def testBackCamcorder720playLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Left_Back_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_720p","Left")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check back camcorder 480p preview in left orientation.
#***************************************************************************************************
    def testBackCamcorder480previewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","SD 480p","Left")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by back camcorder to check playback in left orientation.
#***************************************************************************************************
    def testBackCamcorder480playLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Left_Back_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_480p","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcorder 1080p preview in right orientation.
#***************************************************************************************************
    def testBackCamcorder1080previewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 1080p","Right")
        if self.d(text="HD 1080p", checked="true").exists:
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 1080P video file by back camcorder to check playback in rifht orientation.
#***************************************************************************************************
    def testBackCamcorder1080playRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 1080p")
        self.cameracommon.record_shoot(1,10,"Right_Back_1080p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_1080p","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcorder 720p preview in right orientation.
#***************************************************************************************************
    def testBackCamcorder720previewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","HD 720p","Right")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 720P video file by back camcorder to check playback in rifht orientation.
#***************************************************************************************************
    def testBackCamcorder720playRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Right_Back_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_720p","Right")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check back camcorder 480p preview in right orientation.
#***************************************************************************************************
    def testBackCamcorder480previewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.checkpreview("Back camera video","SD 480p","Right")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by back camcorder to check playback in right orientation.
#***************************************************************************************************
    def testBackCamcorder480playRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamcorder()
        self.cameracommon.switch_quality("Back camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Right_Back_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Back_480p","Right")
        else:
            assert False

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)





class Backcamera(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cameracommon = CameraCommon(self.d, self.serialno)
        self.cameracommon.d = self.d
        self.cameracommon.serialno = self.serialno
        
    def setUp(self):
        self.cameracommon.opencamera()
        print ("setup done")

#***************************************************************************************************
# Description: Change to back Camera.
#***************************************************************************************************
    def testChangetoBackCamera(self):
        self.cameracommon.change_function("Camera")
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle").click()   #open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:   #check if there is change option, change option exists that means there are 2 cameras
                second = True
                if self.d(description="Front camera").exists:                                     
                    self.d(description="Front camera").click.wait()   #set to aim direction option
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                second = False                      #there is only back camera no need to check change result
        except Exception as e:
            print(e)
            raise
        if second == True:    #only when there are 2 cameras to check the change result
            if self.d(description="Back camera").exists:      #check change result
                result = True
            else:
                result = False
            expected_result = True
            self.assertEqual(result, expected_result)
            print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")


#***************************************************************************************************
# Description: Check back camcera 5.0M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera5_0Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 5.0 megapixels","Natural")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Take a 5M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera5_0M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_5.0 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.8M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera0_8Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.8 megapixels","Natural")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.8M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera0_8M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.8 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.3M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera0_3Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.3 megapixels","Natural")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera0_3M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.3 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 2.1M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera2_1Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 2.1 megapixels","Natural")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera2_1M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_2.1 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.9M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera0_9Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 0.9 megapixels","Natural")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera0_9M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.9 megapixels","Natural")
        else: 
            assert False

#***************************************************************************************************
# Description: Check back camcera 1.0M preview in natural orientation.
#***************************************************************************************************
    def testBackCamera1_0Mpreview(self):
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(8:5) 1.0 megapixels","Natural")
        if self.d(text="(8:5) 1.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 1.0M picture file by back camera to check playback in natural orientation.
#***************************************************************************************************
    def testBackCamera1_0M(self):
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(8:5) 1.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_1.0 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 5.0M preview in left orientation.
#***************************************************************************************************
    def testBackCamera5_0MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 5.0 megapixels","Left")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Take a 5M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera5_0MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_5.0 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.8M preview in left orientation.
#***************************************************************************************************
    def testBackCamera0_8MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.8 megapixels","Left")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.8M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera0_8MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.8 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.3M preview in left orientation.
#***************************************************************************************************
    def testBackCamera0_3MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.3 megapixels","Left")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera0_3MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.3 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 2.1M preview in left orientation.
#***************************************************************************************************
    def testBackCamera2_1MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 2.1 megapixels","Left")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera2_1MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_2.1 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.9M preview in left orientation.
#***************************************************************************************************
    def testBackCamera0_9MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 0.9 megapixels","Left")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera0_9MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.9 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 1.0M preview in left orientation.
#***************************************************************************************************
    def testBackCamera1_0MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(8:5) 1.0 megapixels","Left")
        if self.d(text="(8:5) 1.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
            
#***************************************************************************************************
# Description: Take a 1.0M picture file by back camera to check playback in left orientation.
#***************************************************************************************************
    def testBackCamera1_0MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(8:5) 1.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_1.0 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 5.0M preview in right orientation.
#***************************************************************************************************
    def testBackCamera5_0MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 5.0 megapixels","Right")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 5M picture file by back camera to check playback in right orientation.
#***************************************************************************************************
    def testBackCamera5_0MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_5.0 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.8M preview in right orientation.
#***************************************************************************************************
    def testBackCamera0_8MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.8 megapixels","Right")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
            
#***************************************************************************************************
# Description: Take a 0.8M picture file by back camera to check playback in Right orientation.
#***************************************************************************************************
    def testBackCamera0_8MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.8 megapixels","Right")
        else:
            assert False
 
#***************************************************************************************************
# Description: Check back camcera 0.3M preview in right orientation.
#***************************************************************************************************
    def testBackCamera0_3MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(4:3) 0.3 megapixels","Right")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by back camera to check playback in Right orientation.
#***************************************************************************************************
    def testBackCamera0_3MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.3 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 2.1M preview in right orientation.
#***************************************************************************************************
    def testBackCamera2_1MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 2.1 megapixels","Right")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by back camera to check playback in Right orientation.
#***************************************************************************************************
    def testBackCamera2_1MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_2.1 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 0.9M preview in right orientation.
#***************************************************************************************************
    def testBackCamera0_9MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(16:9) 0.9 megapixels","Right")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by back camera to check playback in Right orientation.
#***************************************************************************************************
    def testBackCamera0_9MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_0.9 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check back camcera 1.0M preview in right orientation.
#***************************************************************************************************
    def testBackCamera1_0MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.checkpreview("Back camera photo","(8:5) 1.0 megapixels","Right")
        if self.d(text="(8:5) 1.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 1.0M picture file by back camera to check playback in Right orientation.
#***************************************************************************************************
    def testBackCamera1_0MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoBackCamera()
        self.cameracommon.switch_quality("Back camera photo","(8:5) 1.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Back_1.0 megapixels","Right")
        else:
            assert False

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)



class Frontcamcorder(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cameracommon = CameraCommon(self.d, self.serialno)
        self.cameracommon.d = self.d
        self.cameracommon.serialno = self.serialno
        
    def setUp(self):
        self.cameracommon.opencamera()
        print ("setup done")
        
#***************************************************************************************************
# Description: Change to front camcorder.
#***************************************************************************************************
    def testChangetoFrontCamcorder(self):
        self.cameracommon.change_function("Video")
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle").click()   #open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:
                if self.d(description="Back camera").exists:                                     
                    self.d(description="Back camera").click.wait()   #set to aim direction option
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                print ("Error: There is no second camera")
                raise
        except Exception as e:
            print(e)
            raise
        if self.d(description="Front camera").exists:      #check change result
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")
        
#***************************************************************************************************
# Description: Check front camcorder 720p preview in natural orientation.
#***************************************************************************************************
    def testFrontCamcorder720preview(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","HD 720p","Natural")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Record a 720P video file by front camcorder to check if the record quality is right.
#***************************************************************************************************
    def testFrontCamcorder720quality(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","HD 720p")
        self.cameracommon.record_shoot(1,3,"Natural_Front_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkquality()
            if (self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 1280 Height: 720" ):
                result = True
                print (" quality is correct")
            else:
                result = False
                print (" quality is incorrect")
            expected_result = True
            self.assertEqual(result, expected_result)
        else:
            assert False
            
#***************************************************************************************************
# Description: Record a 720P video file by front camcorder to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamcorder720playback(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Natural_Front_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_720p","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check front camcorder 480p preview in natural orientation.
#***************************************************************************************************
    def testFrontCamcorder480preview(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","SD 480p","Natural")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by front camcorder to check if the record quality is right.
#***************************************************************************************************
    def testFrontCamcorder480quality(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","SD 480p")
        self.cameracommon.record_shoot(1,3,"Natural_Front_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkquality()
            if (self.d(textContains="Width").text+" "+self.d(textContains="Height").text == "Width: 720 Height: 480"):
                result = True
                print (" quality is correct")
            else:
                result = False
                print (" quality is incorrect")
            expected_result = True
            self.assertEqual(result, expected_result)
        else:
            assert False

#***************************************************************************************************
# Description: Record a 480P video file by front camcorder to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamcorder480playback(self):
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Natural_Front_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_480p","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcorder 720p preview in left orientation.
#***************************************************************************************************
    def testFrontCamcorder720previewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","HD 720p","Left")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 720P video file by Front camcorder to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamcorder720playLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Left_Front_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_720p","Left")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check Front camcorder 480p preview in left orientation.
#***************************************************************************************************
    def testFrontCamcorder480previewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","SD 480p","Left")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by Front camcorder to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamcorder480playLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Left_Front_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_480p","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcorder 720p preview in right orientation.
#***************************************************************************************************
    def testFrontCamcorder720previewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","HD 720p","Right")
        if self.d(text="HD 720p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 720P video file by Front camcorder to check playback in rifht orientation.
#***************************************************************************************************
    def testFrontCamcorder720playRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","HD 720p")
        self.cameracommon.record_shoot(1,10,"Right_Front_720p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_720p","Right")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check Front camcorder 480p preview in right orientation.
#***************************************************************************************************
    def testFrontCamcorder480previewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamcorder()
        self.cameracommon.checkpreview("Front camera video","SD 480p","Right")
        if self.d(text="SD 480p", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Record a 480P video file by Front camcorder to check playback in right orientation.
#***************************************************************************************************
    def testFrontCamcorder480playRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamcorder()
        self.cameracommon.switch_quality("Front camera video","SD 480p")
        self.cameracommon.record_shoot(1,10,"Right_Front_480p_recording")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkplayback("Front_480p","Right")
        else:
            assert False

    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)


class Frontcamera(unittest.TestCase):

    def __init__(self, name, serialno):
        unittest.TestCase.__init__(self, name)
        self.d = Device(serialno)
        self.serialno = serialno
        self.cameracommon = CameraCommon(self.d, self.serialno)
        self.cameracommon.d = self.d
        self.cameracommon.serialno = self.serialno
        
    def setUp(self):
        self.cameracommon.opencamera()
        print ("setup done")
        
#***************************************************************************************************
# Description: Change to front camcera.
#***************************************************************************************************
    def testChangetoFrontCamera(self):
        self.cameracommon.change_function("Camera")
        try:
            self.d(resourceId="com.android.camera2:id/mode_options_toggle").click()   #open change menu
            time.sleep(1)
            if self.d(resourceId="com.android.camera2:id/camera_toggle_button").exists:
                if self.d(description="Back camera").exists:                                     
                    self.d(description="Back camera").click.wait()   #set to aim direction option
                    print("TRY TO SET CAMERA/CAMCORDER DIRECTION")
            else:
                print ("Error: There is no second camera")
                raise
        except Exception as e:
            print(e)
            raise
        if self.d(description="Front camera").exists:      #check change result
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        print("SET CAMERA/CAMCORDER DIRECTION SUCCESSFULLY")
        
#***************************************************************************************************
# Description: Check front camcera 5.0M preview in natural orientation.
#***************************************************************************************************
    def testFrontCamera5_0Mpreview(self):
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 5.0 megapixels","Natural")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 5M picture file by Front camera to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamera5_0M(self):
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_5.0 megapixels","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check front camcera 0.8M preview in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_8Mpreview(self):
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.8 megapixels","Natural")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.8M picture file by Front camera to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_8M(self):
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.8 megapixels","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check front camcera 0.3M preview in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_3Mpreview(self):
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.3 megapixels","Natural")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by Front camera to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_3M(self):
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.3 megapixels","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check front camcera 2.1M preview in natural orientation.
#***************************************************************************************************
    def testFrontCamera2_1Mpreview(self):
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 2.1 megapixels","Natural")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by Front camera to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamera2_1M(self):
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_2.1 megapixels","Natural")
        else:
            assert False
            
#***************************************************************************************************
# Description: Check front camcera 0.9M preview in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_9Mpreview(self):
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 0.9 megapixels","Natural")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by Front camera to check playback in natural orientation.
#***************************************************************************************************
    def testFrontCamera0_9M(self):
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.9 megapixels","Natural")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 5.0M preview in left orientation.
#***************************************************************************************************
    def testFrontCamera5_0MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 5.0 megapixels","Left")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
        
#***************************************************************************************************
# Description: Take a 5M picture file by Front camera to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamera5_0MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_5.0 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 0.8M preview in left orientation.
#***************************************************************************************************
    def testFrontCamera0_8MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.8 megapixels","Left")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.8M picture file by Front camera to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamera0_8MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.8 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 0.3M preview in left orientation.
#***************************************************************************************************
    def testFrontCamera0_3MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.3 megapixels","Left")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by Front camera to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamera0_3MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.3 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 2.1M preview in left orientation.
#***************************************************************************************************
    def testFrontCamera2_1MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 2.1 megapixels","Left")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by Front camera to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamera2_1MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_2.1 megapixels","Left")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 0.9M preview in left orientation.
#***************************************************************************************************
    def testFrontCamera0_9MpreviewLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 0.9 megapixels","Left")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by Front camera to check playback in left orientation.
#***************************************************************************************************
    def testFrontCamera0_9MLeft(self):
        self.cameracommon.rotate("1")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.9 megapixels","Left")
        else:
            assert False


#***************************************************************************************************
# Description: Check Front camcera 5.0M preview in right orientation.
#***************************************************************************************************
    def testFrontCamera5_0MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 5.0 megapixels","Right")
        if self.d(text="(4:3) 5.0 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 5M picture file by Front camera to check playback in right orientation.
#***************************************************************************************************
    def testFrontCamera5_0MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 5.0 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_5.0 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 0.8M preview in right orientation.
#***************************************************************************************************
    def testFrontCamera0_8MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.8 megapixels","Right")
        if self.d(text="(4:3) 0.8 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)
            
#***************************************************************************************************
# Description: Take a 0.8M picture file by Front camera to check playback in Right orientation.
#***************************************************************************************************
    def testFrontCamera0_8MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.8 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.8 megapixels","Right")
        else:
            assert False
 
#***************************************************************************************************
# Description: Check Front camcera 0.3M preview in right orientation.
#***************************************************************************************************
    def testFrontCamera0_3MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(4:3) 0.3 megapixels","Right")
        if self.d(text="(4:3) 0.3 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.3M picture file by Front camera to check playback in Right orientation.
#***************************************************************************************************
    def testFrontCamera0_3MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(4:3) 0.3 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.3 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 2.1M preview in right orientation.
#***************************************************************************************************
    def testFrontCamera2_1MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 2.1 megapixels","Right")
        if self.d(text="(16:9) 2.1 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 2.1M picture file by Front camera to check playback in Right orientation.
#***************************************************************************************************
    def testFrontCamera2_1MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 2.1 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        time.sleep(2)
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_2.1 megapixels","Right")
        else:
            assert False

#***************************************************************************************************
# Description: Check Front camcera 0.9M preview in right orientation.
#***************************************************************************************************
    def testFrontCamera0_9MpreviewRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.checkpreview("Front camera photo","(16:9) 0.9 megapixels","Right")
        if self.d(text="(16:9) 0.9 megapixels", checked="true").exists:  
            result = True
        else:
            result = False
        expected_result = True
        self.assertEqual(result, expected_result)

#***************************************************************************************************
# Description: Take a 0.9M picture file by Front camera to check playback in Right orientation.
#***************************************************************************************************
    def testFrontCamera0_9MRight(self):
        self.cameracommon.rotate("3")
        self.testChangetoFrontCamera()
        self.cameracommon.switch_quality("Front camera photo","(16:9) 0.9 megapixels")
        self.cameracommon.record_shoot(2,0,"none")
        if self.d(resourceId="com.android.camera2:id/rounded_thumbnail_view").wait.exists(timeout = 1000):
            self.cameracommon.checkpicture("Front_0.9 megapixels","Right")
        else:
            assert False
        
    def tearDown(self):
        try:
            self.d.press.back()
            self.d.press.home()
            time.sleep(1)
            print("tearDown")
        except Exception as e:
            print(e)
